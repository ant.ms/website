function updateColorTheme() {
  const theme = localStorage.getItem("theme");

  if (["light", "dark"].includes(theme))
    document.body.setAttribute("data-theme", theme);
  else {
    if (!window.matchMedia) {
      document.body.setAttribute("data-theme", "dark");
    } else {
      const preference = window.matchMedia("(prefers-color-scheme: dark)");
      document.body.setAttribute(
        "data-theme",
        preference.matches ? "dark" : "light",
      );
      preference.addEventListener("change", (e) => {
        document.body.setAttribute("data-theme", e.matches ? "dark" : "light");
      });
    }
  }
}
updateColorTheme();
