window.addEventListener("DOMContentLoaded", (event) => {
  document.querySelectorAll("img").forEach((element) => {
    if (element.onclick == null)
      element.onclick = () => showImageToast(element.src);
    element.classList.add("clickable");
  });
  console.log("made images clickable");
});
