const typeWriter = async () => {
  let speed = 115;

  const title = document.querySelector("#home-center > h1");
  const titleText = title.innerText;
  title.innerText = "";

  const subtitle = document.querySelector("#home-center > p");
  const subtitleText = subtitle.innerText;
  subtitle.innerText = "";

  await new Promise((r) => setTimeout(r, 250));

  for (let i = 0; i < titleText.length + titleText.split(" ").length; i++) {
    title.innerText = titleText.substring(0, i);
    await new Promise((r) => setTimeout(r, speed));
  }

  speed = 90;

  const socials = document.querySelector("#home-center > div:last-child");
  socials.style.opacity = 1;
  socials.style.height = "90px";

  for (let i = 0; i < subtitleText.length; i++) {
    if (subtitleText[i] == " ") i++;
    subtitle.innerText = subtitleText.substring(0, i);
    await new Promise((r) => setTimeout(r, speed));
  }
};
typeWriter();

const thumbnail3d = () => {
  const card = document.querySelector("#thumbnail");
  let bounds;

  function rotateToMouse(e) {
    const mouseX = e.clientX;
    const mouseY = e.clientY;
    const leftX = mouseX - bounds.x;
    const topY = mouseY - bounds.y;
    const center = {
      x: leftX - bounds.width / 2,
      y: topY - bounds.height / 2,
    };
    const distance = Math.sqrt(center.x ** 2 + center.y ** 2);

    card.querySelector("img").style.transform = `
      scale3d(1.07, 1.07, 1.07)
      rotate3d(
        ${center.y / 100},
        ${-center.x / 100},
        0,
        ${Math.log(distance) * 2}deg
      )
    `;
  }

  card.addEventListener("mouseenter", () => {
    bounds = card.getBoundingClientRect();
    document.addEventListener("mousemove", rotateToMouse);
  });

  card.addEventListener("mouseleave", () => {
    document.removeEventListener("mousemove", rotateToMouse);
    card.querySelector("img").style.transform = "";
    card.style.background = "";
  });
};
thumbnail3d();
