const timeAgo = (dateParam) => {
  const date = typeof dateParam === "object" ? dateParam : new Date(dateParam);
  const seconds = Math.round((new Date() - date) / 1000);
  const minutes = Math.round(seconds / 60);

  if (seconds < 60)
    return `${Math.floor(seconds)} second${
      Math.floor(seconds) != 1 ? "s" : ""
    }`;
  else if (seconds < 90) return "about a minute";
  else if (minutes < 60)
    return `${Math.floor(minutes)} minute${
      Math.floor(minutes) != 1 ? "s" : ""
    }`;
  else if (minutes < 1440)
    return `${Math.floor(minutes / 60)} hour${
      Math.floor(minutes / 60) != 1 ? "s" : ""
    }`;

  return `${Math.floor(minutes / 1440)} day${
    Math.floor(minutes / 1440) != 1 ? "s" : ""
  }`;
};
