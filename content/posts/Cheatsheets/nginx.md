---
title: "Cheatsheet: Nginx + Certbot"
date: 2020-05-13T10:57:42+02:00
draft: false
categories:
  - Snippets
tags:
  - Nginx
  - Server
  - Help
icon: https://cdn.ant.lgbt/icons/nginx.png
banner: archived
---

Configuring NGINX can be really hard at times, in order for me not needing to scavange the web for these code-snippets.

<!--more-->

This command creates a LetsEncrypt-SSL-Certificate for every subdomain (sub.example.com) and the domain (example.com) with the use of two DNS-entries.

```bash
certbot certonly --manual --preferred-challenges=dns --email <yourEmail> --server https://acme-v02.api.letsencrypt.org/directory --agree-tos -d *.<yourDomain> -d <yourDomain>
```

This snippet allows the user to enter a subdomain and be redirected to the folder /var/www/example.com/_enteredSubDomain_. This is really useful in a lot of applications. It also handles error pages and redirects them to e.g. /var/www/_404.html_.

```nginx
server {
    server_name ~^(?P<sub>.+)\.example\.com$;
    root /var/www/example.com/$sub;

    error_page 403 /error/403.html;
    error_page 404 /error/404.html;

    location ^~ /error/ {
            internal;
            alias /var/www/;
    }

    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/ant.lgbt/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/ant.lgbt/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}
```
