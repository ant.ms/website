---
title: "Cheatsheet: Gitlab"
date: 2020-06-01T10:07:42+02:00
draft: false
categories:
  - Snippets
tags:
  - Git
  - Gitlab
  - Cheatsheet
  - Help
icon: https://cdn.ant.lgbt/icons/gitlab.png
---

These are the same commands that gitlab shows you when you create a repository. I just posted this post here, because it's much more convenient to have very quick access to them right here.

# Git global setup

```bash
git config --global user.name "Yanik Ammann"
git config --global user.email "confused@ant.lgbt"
```

# Repository Management

## Push an existing folder

```bash
git init
git remote add origin git@gitlab.com:ConfusedAnt/CHANGE-ME.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

> you may also want to add a _.gitignore_-file from [here](https://www.gitignore.io/);

## Push an existing Git repository

```bash
git remote rename origin old-origin
git remote add origin git@gitlab.com:ConfusedAnt/CHANGE-ME.git
git push -u origin --all
git push -u origin --tags
```

# SSH-Keys

Run these commands:

```bash
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

And the navigate to [gitlab.com/profile/keys](https://gitlab.com/profile/keys) and add the SSH-Key.

# GPG-Keys

Run these commands to generate and list the key:

```bash
gpg --full-gen-key
gpg --list-secret-keys --keyid-format LONG <your-email>
gpg --armor --export <your-key-id> # e.g.30F2B65B9246B6CA
```

And the navigate to [gitlab.com/profile/gpg_keys](https://gitlab.com/profile/gpg_keys) and add the GPG-Key.

You may also want to set the GPG-Key to be used in every commit.

```bash
git config --global commit.gpgsign true
```
