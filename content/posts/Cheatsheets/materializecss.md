---
title: "Cheatsheet: Materializecss (HTML)"
date: 2020-05-13T11:00:42+02:00
draft: false
categories:
  - Snippets
tags:
  - HTML
  - Material
  - Materializecss
  - Materialize
  - CSS
  - Cheatsheet
  - Help
icon: https://cdn.ant.lgbt/icons/materializecss.png
banner: archived
---

[Materializecss](https://materializecss.com/) is an amazing JS and CSS Framework.

<!--more-->

Here is a default template that uses the CDNs, it includes a Navbar with Icons and a container for your content.

```html
<!doctype html>
<html>
  <head>
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"
    />
    <link
      rel="stylesheet"
      href="https://fonts.googleapis.com/icon?family=Material+Icons"
    />

    <title>Your Website-Title</title>

    <meta charset="UTF-8" />
    <meta name="description" content="Your Website-Description" />
    <meta name="keywords" content="HTML, CSS, JavaScript, Material" />
    <meta name="author" content="Yanik Ammann" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  </head>

  <body>
    <nav>
      <div class="nav-wrapper" style="margin-left: 15px">
        <a href="." class="brand-logo left" style="margin-left: 15px"
          ><i class="material-icons">cloud</i
          ><span class="hide-on-small-only">Your Website-Title</span></a
        >
        <ul class="right">
          <li>
            <a href="#"><i class="material-icons">search</i></a>
          </li>
          <li>
            <a href="#"><i class="material-icons">view_module</i></a>
          </li>
          <li>
            <a href="#"><i class="material-icons">refresh</i></a>
          </li>
          <li>
            <a href="#"><i class="material-icons">more_vert</i></a>
          </li>
        </ul>
      </div>
    </nav>

    <div style="height: 10px"></div>

    <div class="container">Page Content goes here</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>
```

Alternatively you can use this command to create a complete project file and curl the CSS and JS files (make sure that you execute this in an empty folder):

```bash
curl -sSL cdn.ant.lgbt/files/material/script.sh
```
