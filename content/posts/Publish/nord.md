---
title: "Publish: Nord Colorscheme Cheatsheet"
date: 2020-04-05T13:35:41+02:00
draft: false
categories:
tags:
  - CSS
  - Color
  - Themes
icon: https://repository-images.githubusercontent.com/67354143/7df80a00-6156-11e9-8698-a95efdedc658
---

The Nord colorsheme is one of my favourite, actually... it is **the** best one I've ever seen and a lot of stuff supports it (including this website btw). You can copy the color-codes to clipboard by clicking on them.

> This is copy paste from [www.nordtheme.com/docs/colors-and-palettes](https://www.nordtheme.com/docs/colors-and-palettes) and is only really here as a cheatsheet, so I can get to it quicker and more easily

Quick Jump to: [Polar Night](#polar-night), [Snow Storm](#snow-storm), [Frost](#frost), or [Aurora](#aurora)

---

Polar Night is made up of four darker colors that are commonly used for base elements like backgrounds or text color in bright ambiance designs.

# Polar Night

<table>

<tr>
<td style="background-color:#2e3440;color: #e5e9f0"  onclick="copyToClipboard('#2e3440')">#2e3440</td>
<td>
For dark ambiance designs, it is used for background and area coloring while it's not used for syntax highlighting at all because otherwise it would collide with the same background color.

For bright ambiance designs, it is used for base elements like plain text, the text editor caret and reserved syntax characters like curly- and square brackets.

It is rarely used for passive UI elements like borders, but might be possible to achieve a higher contrast and better visual distinction (harder/not flat) between larger components.

</td>
</tr>

<tr>
<td style="background-color:#3b4252;color: #e5e9f0" onclick="copyToClipboard('#3b4252')">#3b4252</td>
<td>
For dark ambiance designs it is used for elevated, more prominent or focused UI elements like

- **status bars and text editor gutters**
- panels, modals and floating popups like **notifications or auto completion**
- user interaction/form components like **buttons, text/select fields or checkboxes**

It also works fine for more inconspicuous and passive elements like borders or as dropshadow between different components.
There's currently no official port project that makes use of it for syntax highlighting.

For bright ambiance designs, it is used for more subtle/inconspicuous UI text elements that do not need so much visual attention.
Other use cases are also state animations like a more brighter text color when a button is hovered, active or focused.

</td>
</tr>

<tr>
<td style="background-color:#434c5e;color: #e5e9f0" onclick="copyToClipboard('#434c5e')">#434c5e</td>
<td>
For dark ambiance designs, it is used to colorize the currently active text editor line as well as selection- and text highlighting color.
For both bright & dark ambiance designs it can also be used as an brighter variant for the same target elements like nord1.
</td>
</tr>

<tr>
<td style="background-color:#4c566a;color: #e5e9f0" onclick="copyToClipboard('#4c566a')">#4c566a</td>
<td>
For dark ambiance designs, it is used for UI elements like indent- and wrap guide marker.
In the context of code syntax highlighting it is used for comments and invisible/non-printable characters.

For bright ambiance designs, it is, next to nord1 and nord2 as darker variants, also used for the most subtle/inconspicuous UI text elements that do not need so much visual attention.

</td>
</tr>

</table>
<br>

# Snow Storm

Snow Storm is made up of three bright colors that are commonly used for text colors or base UI elements in bright ambiance designs.

The palette can be used for two different shading ambiance styles:

bright to dark — This is the recommended style that uses nord6 as base color and highlights other UI elements with nord5 and nord4.
dark to bright (semi-light) — This style uses nord4 as base color and highlights other UI elements with nord5 and nord6.

The documentation of the different colors from this palette are based on the recommended bright to dark ambiance style.
To apply the color purposes to the dark to bright style the definitions can be used in reversed order.

<table>

<tr>
<td style="background-color:#d8dee9;color: #2e3440" onclick="copyToClipboard('#d8dee9')">#d8dee9</td>
<td>
For dark ambiance designs, it is used for background and area coloring while it's not used for syntax highlighting at all because otherwise it would collide with the same background color.

For bright ambiance designs, it is used for base elements like plain text, the text editor caret and reserved syntax characters like curly- and square brackets.

It is rarely used for passive UI elements like borders, but might be possible to achieve a higher contrast and better visual distinction (harder/not flat) between larger components.

</td>
</tr>

<tr>
<td style="background-color:#e5e9f0;color: #2e3440" onclick="copyToClipboard('#e5e9f0')">#e5e9f0</td>
<td>
For dark ambiance designs, it is used to colorize the currently active text editor line as well as selection- and text highlighting color.
For both bright & dark ambiance designs it can also be used as an brighter variant for the same target elements like nord1.
</td>
</tr>

<tr>
<td style="background-color:#eceff4;color: #2e3440" onclick="copyToClipboard('#eceff4')">#eceff4</td>
<td>
For dark ambiance designs, it is used for UI elements like indent- and wrap guide marker.
In the context of code syntax highlighting it is used for comments and invisible/non-printable characters.

For bright ambiance designs, it is, next to nord1 and nord2 as darker variants, also used for the most subtle/inconspicuous UI text elements that do not need so much visual attention.

</td>
</tr>

</table>
<br>

# Frost

Frost can be described as the heart palette of Nord, a group of four bluish colors that are commonly used for primary UI component and text highlighting and essential code syntax elements.

All colors of this palette are used the same for both dark & bright ambiance designs.

<table>

<tr>
<td style="background-color:#8fbcbb;color: #e5e9f0" onclick="copyToClipboard('#8fbcbb')">#8fbcbb</td>
<td>
A calm and highly contrasted color reminiscent of frozen polar water.

Used for UI elements that should, next to the primary accent color nord8, stand out and get more visual attention.
In the context of syntax highlighting it is used for classes, types and primitives.

</td>
</tr>

<tr>
<td style="background-color:#88c0d0;color: #e5e9f0" onclick="copyToClipboard('#88c0d0')">#88c0d0</td>
<td>
The bright and shiny primary accent color reminiscent of pure and clear ice.

Used for primary UI elements with main usage purposes that require the most visual attention.
In the context of syntax highlighting it is used for declarations, calls and execution statements of functions, methods and routines.

</td>
</tr>

<tr>
<td style="background-color:#81a1c1;color: #e5e9f0" onclick="copyToClipboard('#81a1c1')">#81a1c1</td>
<td>
A more darkened and less saturated color reminiscent of arctic waters.

Used for secondary UI elements that also require more visual attention than other elements.
In the context of syntax highlighting it is used for language specific, syntactic and reserved keywords as well as

- support characters
- operators
- tags
- units
- punctuations like (semi)colons, points and commas
</td>
</tr>

<tr>
<td style="background-color:#5e81ac;color: #e5e9f0" onclick="copyToClipboard('#5e81ac')">#5e81ac</td>
<td>
A dark and intensive color reminiscent of the deep arctic ocean.

Used for tertiary UI elements that require more visual attention than default elements.
In the context of syntax highlighting it is used for pragmas, comment keywords and pre-processor statements.

</td>
</tr>

</table>
<br>

# Aurora

Aurora consists of five colorful components reminiscent of the „Aurora borealis“, sometimes referred to as polar lights or northern lights.

All colors of this palette are used the same for both dark & bright ambiance designs.

<table>

<tr>
<td style="background-color:#bf616a;color: #e5e9f0" onclick="copyToClipboard('#bf616a')">#bf616a</td>
<td>
Used for UI elements that are rendering error states like linter markers and the highlighting of Git diff deletions.
In the context of syntax highlighting it is used to override the highlighting of syntax elements that are detected as errors.
</td>
</tr>

<tr>
<td style="background-color:#d08770;color: #e5e9f0" onclick="copyToClipboard('#d08770')">#d08770</td>
<td>
Rarely used for UI elements, but it may indicate a more advanced or dangerous functionality.
In the context of syntax highlighting it is used for special syntax elements like annotations and decorators.
</td>
</tr>

<tr>
<td style="background-color:#ebcb8b;color: #e5e9f0" onclick="copyToClipboard('#ebcb8b')">#ebcb8b</td>
<td>
Used for UI elements that are rendering warning states like linter markers and the highlighting of Git diff modifications.
In the context of syntax highlighting it is used to override the highlighting of syntax elements that are detected as warnings as well as escape characters and within regular expressions.
</td>
</tr>

<tr>
<td style="background-color:#a3be8c;color: #e5e9f0" onclick="copyToClipboard('#a3be8c')">#a3be8c</td>
<td>
Used for UI elements that are rendering success states and visualizations and the highlighting of Git diff additions.
In the context of syntax highlighting it is used as main color for strings of any type like double/single quoted or interpolated.
</td>
</tr>

<tr>
<td style="background-color:#b48ead;color: #e5e9f0" onclick="copyToClipboard('#b48ead')">#b48ead</td>
<td>
Rarely used for UI elements, but it may indicate a more uncommon functionality.
In the context of syntax highlighting it is used as main color for numbers of any type like integers and floating point numbers.
</td>
</tr>

</table>

<style>
td:first-child {
    width: 10rem;
    text-align: center;
}
</style>
