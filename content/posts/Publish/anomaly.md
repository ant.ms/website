---
title: "Taking up a Secondary Job at Anomaly"
date: 2022-06-01T20:10:47+02:00
draft: false
categories:
  - Change
tags:
  - life
  - work
icon: icons/anomaly.png
---

Now before you say: "Yanik! You already have a job!!". First, thank you for your concern. Second, let me explain.

For the ones that don't know, I'm currently doing an apprenticeship at Siemens (it's near the end of my third (out of four) year, at the time of writing this). Nothing about that changes, I'm still doing my apprenticeship and it's my main priority.

As we are having less and less school though (only one day a week now), I've found myself having more and more spare time. So when a good friend of me asked if I wanted to join his company as a part-time developer, I ended up saying yes.

There are definitely a lot of challenges there, that I have not faced before. However, I am confident that I'm going to be able to master those challenges, learn a shit load about how a smaller scale business works, and gather valuable experience for future endeavors.

TLDR: I am now part of (Techlead) Anomaly Science (see [anomsci.com](https://anomsci.com/) for details).
