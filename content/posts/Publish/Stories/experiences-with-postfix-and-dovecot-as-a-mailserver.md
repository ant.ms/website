---
title: "Experiences with Postfix and Dovecot as a mailserver"
date: 2019-12-18T19:02:12+01:00
draft: false
categories:
tags:
  - failed
  - mailserver
  - postfix
  - dovecot
  - linux
  - mailgun
  - pihole
  - server
icon: https://cdn.ant.lgbt/icons/mail.png
---

It sounded like a great idea at first, _hosting your own mailserver_, at first. But, oh boy, is it weird and so not worth it for me.

<!--more-->

I started off like everyone would have by searching for tutorials and software to use for the server. My choice for a WebInterface was quite easy because I already used Nextcloud on a daily basis and it has this handy [email client](https://apps.nextcloud.com/apps/mail) that you can install. But the server itself was a harder choice, after trying multiple I ended up using a combination of Dovecot and Postfix for both IMAP (and POP3) and SMTP. That's easier said then done though and, because I had litteraly zero experience, it ended up taking quite a lot longer to implement than expected but it ended up functioning.

During normal use I then discovered that iCloud-Email-Addresses and Gmail-Addresses seem to block me and that I end up in the spam folder in (more or less) every other mail-provider. Searching for a solution I online, I found [Mailgun](https://www.mailgun.com/). They offer a service where your can relay your outgoing emails (SMTP) throught there server to not make them show up in spam or even get blocked. That worked but I always had issues with it.

So.. that wasn't too bad after all but what I noticed (using my [Pihole](https://pi-hole.net/)) that my server is doing DNS-lookups for weird domains. I didn't think much of it at the time but apparently it's a major problem because someone managed to brute-foce his way into my server and used my server to send spam emails. So basically, I'm going back to my old solution by just using a purchased email-server over at [Nitrado](https://server.nitrado.net/deu/gameserver-mieten) and disabling ssh completely on my server because I'm now on some list that bots constantly try to brute-foce. So I can now completely reset my entire server and everyting that's on it.... yay.

Now, I don't say that it's not worth it for some people but for me, it's definitely not worth the efford and all the security risks that come with it. I'll probably end up using my my mail domain to redirect to another email (probably going to use [ProdonMail](https://protonmail.com/) or something).
