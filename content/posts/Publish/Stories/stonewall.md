---
title: "50 Years since Stonewall"
date: 2019-06-28T09:00:00+01:00
draft: false
categories:
tags:
  - LGBT
  - Pride
  - Stonewall
  - gay
icon: icons/lgbt.png
---

In order to get this out of the way; This post does not contain any of the usual stuff I post (like my personal projects or something), so if you are only here for those, you are free to skip to another post or do something else entirely.

<!--more-->

![Stonewall](https://cdn.ant.lgbt/img//stonewall.png)

It's now exactly 50 years since the [Stonewall uprising](https://en.wikipedia.org/wiki/Stonewall_riots) in New York (For anyone that is not familiar with then event, I highly recommend you check out it's [Wikipedia-page](https://en.wikipedia.org/wiki/Stonewall_riots) and (if you want) [Stonewall Forever](https://stonewallforever.org/)) which lead to the first pride marches.

I just wanted to thank the entire LGBT-community: **you guys are amazing I have so damn much to thank you guys for**.

And for everyone out there, no matter if you are out or still in the closet, be proud, be yourself and have an amazing time.
