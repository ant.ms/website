---
title: "DE: Kurzgeschichte - Der Brief "
date: 2020-07-28T14:44:42+02:00
draft: false
categories:
  - School
tags:
  - German
icon: https://cdn.ant.lgbt/icons/mail.png
banner: archived
---

This has been a very small story, that I wrote for a small competition from my school. It's horribly written and really doesn't deserve to be published, but I thought that maybe one of you might at least find it a tiny bit enjoyable.

# Der Brief

Es war ein verregneter Nachmittag hier im Dorf. Die Strassen waren leer, was wohl hauptsächlich an den aktuellen Quarantäne-Reglungen lag. Man muss aber auch gestehen, dass hier doch auch sonst nie irgendwas los ist.

Ich war tief in mein Buch versunken, als es plötzlich an der Türe klingelte. Ich ignorierte den kläglichen Versuch der Türklinke mich von meinem Buch loszureissen, doch dann klingelte es erneut und dann noch mal. Langsam begann ich mein Buch zu schliessen und machte mich auf den Weg zur Türe. Mit Verwunderung stellte ich jedoch fest, dass da gar niemand war. Ich hatte bereits begonnen die Türe wieder zu schliessen, als mir ein Briefumschlag auf der Türmatte auffiel.

Er war mit meinem Namen adressiert und sah aus, als hätte er schon einiges ertragen müssen. Die Briefmarke war aus dem Jahre 1910, eindeutig ein Sammlerstück. Der Briefumschlag sah nicht gerade neuer aus.

Da ich unterdessen schon komplett durchnässt war, beschloss ich zurück ins Haus zu gehen und den Brief dort weiter zu untersuchen. Ich holte mir ein Messer aus der Küchen und begann den Brief vorsichtig zu öffnen. Er enthielt ein Stück Papier, jedoch nicht nur irgendein handelsübliches Papier. Es sah alt aus, die Ränder waren gekrümmt und das Papier hatte eine beige Farbgebung. Der untere rechte Rand scheint etwas angebrennt zu sein und die Schrift war handgeschrieben. Ich erkannte die Schrift jedoch nicht.

Der Versuch die Schrift zu lesen war mit Komplikationen versehrt:

> Guten Morgen J--i--n -c---
> Wie Sie bereits mitbekommen haben, hat der Rat der fünf Mac----e Sie für einen Besuch bei der Veranstaltung der sieben Da---n eingeladen. Bitte Treffen Sie uns am Stand 7 der 'World Science Fair' am Dienstag dem 3. A-----.
> Wie wünschen Ihnen ---- ein schönes neues 1907.
> Fr....e.y

Ich versuchte weiterhin die Schrift zu entziffern, als mich plötzlich ein Mann in einem professionellen Anzug mich anrannte. Er entschuldigte sich dafür und rannte wieder weiter. Er wirkte sehr gestresst.

Ich fuhr also mit dem Entziffern der Schrift fort und... Moment, ich bin doch in meinem Haus. Wo kommt dann dieser Mann her. Ich blickte also auf und bemerke, dass ich inmitten eines offenen Platzes stand. Etwa 20 Menschen waren um mich herum, alle vornehm gekleidet. Die Sonne schien, obwohl es ja vor 5 Minuten noch geregnet hat. Ich nahm mein Handy aus der Tasche und wählte die Nummer meines Kollegen. Jedoch bemerkte ich schnell, dass ich keinen Empfang zu haben scheine.

Sehr verwirrt, inspizierte ich meine Umgebung und erblickte etwas, dass wie eine Informationstafel aussah. Ich bewegte mich zu dieser Tafel und lass in grossen Buchstaben: "Welcome to the World Science Fair of 1906" und darunter einen Kalender vom August 1906.
