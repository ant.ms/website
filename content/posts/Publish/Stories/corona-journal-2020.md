---
title: "Quarantine Journal 2020"
date: 2020-05-01T21:02:12+02:00
draft: false
categories:
tags:
  - corona
  - quarantine
  - journal
  - wtf
icon: https://cdn1.iconfinder.com/data/icons/metro-ui-dock/512/Journal.png
---

This is kind of a joke... okay, not kind of: It **is** a joke. So... what this here should be is just a way for me to spend my spare time. I'm not going into why I'm doing this because I honestly don't even know, nobody is going to read this anyways so yeah.... "Hey nobody, nice that you have time for me".

I'll also include a quote that either me or someone else wrote, if it fits. Also... if you read this (which I find highly unlikely) **PLEASE MESSAGE ME ON DISCORD** (or through email if you'd prefer) **I'M SO FREAKING BORED**.

# 12020-04-05 - A journal is a journal

I've been in quarantine for quite a while now, I really hated it in the beginning but it's strange how much I got used to it in so little time. Kinda starting to prefer it to "normal". Awww, normality, you remember that? Wasn't that nice once when not every single government on this planet basically turned into a monarchy overnight and got rid of democracy _"temporarely"_. What's making things worse that this is going to end at some point and I have to return to "normality".... imagine meeting humans... disgusting...

On a positive side though, the weather is getting a lot better... aka: the weather hates me and wants to punish me, because I want to go out with my boyfriend in such a great weather but then I remember that he's far away :()

> Corona is homophobic

# 12020-04-07 - Anybody out there?

Good news, the Universe is still existing... somewhere? It's starting to feel smaller and smaller by the day though, which is only slightly worrying. Especially strange because the Big Bounce is said to be not true, wish it was though. The Big Bounce just feels so.. wholesome and complete.

Anyways, today two packages arrived 1. 10Gbit networking gear, which works really well and 2. My "repaired motherboard"... WHICH IS STILL NOT WORKING!!!!

> If everything has a point, then wouldn't that mean that non of those points actually mattered so that everything is actually pointless?

PS: My boyfriend is cute and I want hugs

# 12020-04-11 - May there be light

Light, the weirdest thing that you never think about. Light is a spectrum of electromagnetic radiation. I know what you're thinking rn and yes, light is really that weird. It raises the question if god was drunk? In my opinion, if there was a god, then them would have all the ways to "get drunk" without any of the sideeffects. I mean... think about it: A being (or multiple) with basically near unlimited technology and knowledge, capable of creating entire universes. Just think about how much they would know about how to get drunk in the most effective way for a second.

> God's drunk

# 12020-04-20 - Stuff does just happen for no reason

Holidays are over now, back to work. No matter that though, there is a lot of stuff that I would still like to do: like... I don't even know. Appart from that I cannot stop thinking about cutie, maybe that's a side effect of the pandemic, there must be some reason for everything here ~~(probably not because no one cares)~~

> Oil is now cheaper than air

# 12020-04-30 - I hate humans sometimes

Basically my CPU arrived, I thought it was being replaced. 1. They sent me back the broken one, 2. THEY BROKE IT EVEN MORE BY BENDING LITTERALY 30 PINS!!!! I'm mad.

> W.T.F.

# 12020-04-30 - **LINUX!!!**

I love it when people get into linux and discover how you can install applications in the terminal. Most know about it but think that it's an over-exaduration or something, but seeing there reaction when they notice that it's actually like this is freaking awesome.

> Tux greats you

# 12020-05-02 - **wtf...**

This entry is special to me, not because of what's in it but rather because of the way it was written.

I'm currently writing this entry on my Android Tablet.... in **vscode**. This isn't anything emulated or anything you might think. Tbf, calling this native wouldn't be correct either. This uses _code-server_ on my server, running in a docker container. My tablet now has a that code-server installed as a WPA (using Chromium) and, honestly, it works really well. Like... my Tablet is soooo slow, so it doesn't even work that badly. I think this will become the main way for me to write articles and maybe even code once I get my Tablet and will be able to use it anywhere with an unlimited Data plan.

It also means that I can now start working on a project and without chaning what software I'm coding with or even having to work with multiple Git-Branches.

> We arrived in the future
