---
title: "Vision of mobile Computing"
date: 2020-03-01T19:10:05+01:00
draft: false
toc: true
categories:
tags:
  - future
  - mobile
  - Android
  - workflow
  - Linux
  - Android Wear
  - Discord
  - Tablet
  - Desktop
  - Smart Watch
  - Watch
  - Google
icon: https://cdn.ant.lgbt/icons/mobile.png
---

There are a lot of different ideas on how you can optimize your computing, this here is my take on my vision of the best setup I'm trying to create. Thanks to my amazing boyfriend for all the help with forming this idea, you're truly awesome &lt;3

<!--more-->

## The general idea behind it

I want to keep things simple, also have fewer devices but those should be working better and be of good quality. So basically "Less but better devices". I also want to keep things mobile, I'll still be keeping my desktop because it's just so freaking much more powerful but I'll be focusing on making sure that I can do (more or less) everything on the go.

### Smart Watch

First, let's get started with the core of the setup.. the Watch. Yes, really, I think in the center of my interactions with technology should be my smartwatch. Let me explain. The watch should be the main way of doing basic functions. First, I can use it to pay (using something like [Google Pay](https://pay.google.com/about/)) at any local payment terminal via my credit card account, so that eliminates my need to have my wallet with my all the time (although, knowing me, I'll probably continue to keep my phone in my backpack, just to have a backup, in case my watch desides to stop working).

#### Entertainment when on the go

My watch will also become my main method of listening to music and podcasts on the go, for that I will require bluetooth headphones of course, but that shouldn't be an issue as every modern smartwatch has Bluetooth functionality built in. I'm still looking at how well LTE connectivity is compatible with smartwatches and podcast clients and stuff like that, but that would be a very neat feature.

#### Messaging

The other function that it will server me is that it's my simple way of answering short messages. That might seem odd and very stupid but it actually works quite well; the keyboard is suprisingly usably (assuming you're using flow input) and if that fails, I can always use voice recognition to type or even send voice messages with that. Of course I won't be sending large messages with it but it should be fine for basic every-day replies and, to be perfectly honest, I'll have my tablet (more on that later) or some kind of Laptop with me most of the time anyways, so I can write the larget chats and conversatinos on there without any issues.

I'm planing on writing my own Discord Client for my Watch ~~technically that's against the terms, but idc~~. Mainly because I haven't been able to find one and because it's not actually that much stuff that I need. I only really need to be able to use my DM's which shouldn't be that hard, and then read the messages in there and write them with keyboard and voice type, maybe even voice messages although idk if I will be able to do that.

## An SSH Based workflow

Work will be mostly be done in an SSH session, running on my server. You may ask why but there are a lot of reasons why this is the way I want to go. First, it is on a server, which means that even on a low-end device (like a tablet/phone/...) I can use all the performance available on the server. Aka: a complete CPU and a lot of memory and storage, plus very fast connectivity to my network and anything in the Internet. Apart from that it has other advantage too, like having the same enviroment on every single device, this gets rid of a lot of different things that might cause issues that I've experienced in the past. For example, I don't have the issue of running out of space anymore, I always have the same version of my files and don't have to check if the synchronisation caused any problems (which does happen and generally you only notice after a long time).

The most significant disadvantage, in my opinion, is that you always have to be connected. So you do need to have an Internet connection (or at least a connection to the server) when you want to get work done. That isn't as big of a problem in most cases because SSH (under a "normal" workload) doesn't require that much bandwidth and I will be getting an unlimited (at lest Switzerland but maybe complete EU) data plan to go with all the other gear so I don't have to worry that much.

### Tablet

I'll (most likely) be buying the Samsung Galaxy Tab S7 (with LTE, pen and keyboard) as my main productivity device for a variety of reasons. First it has LTE, so I don't have to take my phone with me for mobile internet or hunt down public WiFi-networks. It also has a really good battery live and the screen is probably going ot be amazing. The pen is also really useful because sometimes you really just want to draw something and having a thin tablet with a great pin is really helpful.

I've also been considering the [Microsoft Surface](https://www.microsoft.com/en-us/surface) but decided against it because: first, it's Microsoft, and second, it uses a lot more power and can't be charged from a "normal" powerbank and it's more expensive (more or less).

### Desktop

My desktop should just be my main Workstation, I'll be doing a lot of my work on here that can benefit from the extra performance ~~aka: wasting my time playing weird games~~. All my files from my server are either directly mounted or at least synced with it (using [Syncthing](https://syncthing.net/), which is freaking awesome) so I will still have the same files to work with.

There is not that much to be said about it, it's just a normal desktop that will be used most of the time when I'm at home.
