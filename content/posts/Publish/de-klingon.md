---
title: "DE: Basic Klingon language"
date: 2020-02-05T17:27:42+01:00
draft: false
categories:
  - German
tags:
  - German
  - language
  - lolz
icon: icons/klingon.png
---

Klingon language may not be well known, but it's certainly a lot of fun in my opinion.

<!--more-->

For everyone that doesn't know, read up on the Klingon language on [kli.org](https://www.kli.org/).

![klingon](https://i.chzbgr.com/full/6239890432/h56640B42/)

| Every-Day                            |                          |
| ------------------------------------ | ------------------------ |
| Ja                                   | HISlaH / HIja'           |
| Nein                                 | ghobe „Kobe“             |
| Kein Problem                         | qaybe                    |
| ok, ich werde es machen              | lu' / luq                |
| nein, ich werde es nich machen       | Qo'                      |
| Erfolg! (Verabschidgung)             | Qapla‘                   |
| Heute ist ein guter Tag zum sterben. | tIhIngan maH!            |
| Was willst du? (Begrüßung)           | nuqneH                   |
| Was passiert hier?                   | qaStaH nuq?              |
| Ich verstehe                         | jIyaj                    |
| Ich verstehe nicht                   | jIyajbe                  |
| Sprechen Sie Klingonisch?            | tlhIngan Hol Dajatlha    |
| Ich spreche kein Klingonisch         | tlhIngan Hol vIjatlhaHbe |
| Wo ist ein gutes Restaurant?         | nuqDaq oH Qe QaQe        |
| Beam mich an Bord!                   | HIjol                    |
| Komm hierher!                        | HIghoS                   |
| Das ist nicht mein Fehler            | pIch vIghajbe‘           |
| hungrig sein                         | ghung                    |
| Bezahle jetzt!                       | DaH yIDIl                |
| Du lügst.                            | bInep                    |
| Lügt „name“?                         | "dachnep" a „name“       |
| Wie spät ist es?                     | 'arlogh Qoylu'pu?        |
| Sei nicht dumm                       | yIDoghQo'                |
| Seid nicht dumm                      | peDoghQo'                |

<br>

| Allgemeines                           |                                                      |
| ------------------------------------- | ---------------------------------------------------- |
| Das sind gute Nachrichten             | buy' ngop                                            |
| Ich habe es nicht getan.              | vItapube                                             |
| Ich bin ein Klingone.                 | tlhIngan jIH                                         |
| Ich möchte schlafen.                  | jIQong vIneH                                         |
| Feuere die Torpedos ab!               | cha yIbaH qaraDI                                     |
| Wo ist das Badezimmer/die Toilette?   | nuqDaq oH puchpae                                    |
| Ich habe Kopfschmerzen.               | jIwuQ                                                |
| Beeil dich!                           | Tugh                                                 |
| Ist dieser Platz besetzt?             | quSDaQ balua                                         |
| Ich bin Bartträger.                   | rolqengwI‘ jIH                                       |
| Ich habe mich verirrt.                | jIHtaHbogh naDev vISovbe‘                            |
| Wo hast du meine Schokolade hingetan? | nuqDaq yuch Dapol                                    |
| `IwlIj jachjaj                        | (Klinkonischer Trinkspruch: Möge dein Blut schreien) |

<br>

| Befehle            |                  |
| ------------------ | ---------------- |
| yI'el              | Kommt herein     |
| pe'el              | Komm herein      |
| lojmIt yIpoSmoH!   | Öffne die Türe   |
| HIghoS             | Komm hier her    |
| naDevvo' yIghoS    | Geh weg          |
| naDevvo' peghoS    | Geht weg         |
| Halt die Klappe!   | bIjaIh'e' yImev  |
| Haltet die Klappe! | SujatIh'e' yImev |

<br>

| Statusupdates |                                                              |
| ------------- | ------------------------------------------------------------ |
| pItlh         | Erledigt. Fertig.                                            |
| Qo'           | Nein, mache ich nicht. Ich weigere mich. Ich stimme nicht zu |
| SuH / Su'     | Bereit.                                                      |
| u' / luq      | Ja, okay. Mache ich.                                         |
| maj           | Gut. (drückt Zufriedenheit aus MOVED TO... Danke)            |
| majQa'        | Sehr gut. Gut gemacht.                                       |
| mevyap        | Stopp. Es reicht. Genug.                                     |

<br>

| Flüche    |                                                                         |
| --------- | ----------------------------------------------------------------------- |
| baQa'     | (nicht übersetzbar, Fluch)                                              |
| ghay'cha' | (nicht übersetzbar, Fluch)                                              |
| ghuy'     | Allgemeiner Fluch, in etwa "Verdammt!"                                  |
| ghuy'cha' | So etwas könnte nach Erhalt einer unangenehmen Nachricht gesagt werden. |
| Hu'tegh   | (nicht übersetzbar, Fluch)                                              |
| QI'yaH    | Recht vulgär, das ist einer der stärksten, unflätigsten Flüche.         |
| Qu'vatlh  | Wird in Momenten äußersten Zorns gesagt.                                |
| va        | Kurzform von Qu'vatlh                                                   |

<br>

| Beleidigungen |                                                          |
| ------------- | -------------------------------------------------------- |
| petaQ         | (nicht übersetzbar, Beleidigung)                         |
| Qovpatlh      | (nicht übersetzbar, Beleidigung)                         |
| taHqeq        | eine klassische Beleidigung                              |
| toDSaH        | verwendet wenn man mit jemandem nicht gut auskommen kann |
| yIntagh       | (nicht übersetzbar, Beleidigung)                         |

---

## Aussprache

Kleinbuchstaben „b,“ „ch,“ „j,“ „l,“ „m,“ „n,“ „p,“ „t,“ „v,“ und „w“ werden im Klingonischen genauso ausgesprochen wie auf Deutsch

- Ein kleines „a“ ist ein langes „a“, wie in „Abend“ <br>
- Ein kleines „e“ ist ein kurzes „e“. so wie in „Bett“ <br>
- Ein großes „I“ wird wie ein kurzes „i“ ausgesprochen wie in „Bitte“ <br>
- Ein kleines „o“ wird wie ein langes offenes „o“ ausgesprochen, wie in „Note“ oder in „Lob“ <br>
- Ein kleines „u“ ist ein langes „u“ wie in „Schule“ oder „Uhu“ <br>
- Ein großes „D“ ist ähnlich dem deutschen „D“, aber du musst die Zunge bei der Aussprache am höchsten Punkt in deinem Gaumen haben, und nicht hinter den Zähnen, wie im Deutschen <br>
- Ein großes "H" ist ein harsches kehliges „h“, wie etwa in „Bach“ aber unbetont, „das „gh“ ist im Klingonischen ein Buchstabe und ist wie das „H“, aber betont und sollte tief aus dem Rachen kommen <br>
- Das „ng“ ist ein einzelner Buchstabe, wird aber genauso gesprochen wie „ng“ in „Eng“ <br>
- Ein kleines „q“ ist ähnlich wie ein deutsches „k“, wird aber weiter hinten in der Kehle geformt. Deine Zunge sollte die Uvula oder Halsöffnung berühren wenn du es formst. Ein großes „Q“ ist ähnlich, muss aber sofort von einem großen „H“ gefolgt sein <br>
- Ein kleines "r" ist wie das deutsche gerollte „r“ <br>
- Ein großes „S“ ist ähnlich wie ein „sch“, aber mit der Zunge weiter oben am Gaumen <br>
- Das „tlh“ ist ein einzelner Buchstabe im Klingonischen. Beginne mit einem „t“ Klang, aber lasse die Zunge dabei auf die Mundseite fallen, statt nach unten. Von dort stoße das „l“ aus <br>
- Ein kleines „y“ wird wie „j“ ausgesprochen, wie in „jetzt“ <br>
- Der Einzelapostroph (') ist im Klingonischen ein Buchstabe. Es ist der gleiche Klang wie ein langes „ah“ oder „uh“ am Wortanfang und im Prinzip die kurze Ausklingpause in der Kehle. Im Klingonischen kann dies auch in der Mitte des Wortes genutzt werden
