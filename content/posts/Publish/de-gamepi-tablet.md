---
title: "DE: Abschlussarbeit - GamePi-Tablet"
date: 2019-03-27T11:36:47+01:00
draft: false
toc: true
categories:
  - Object
tags:
  - work
  - German
  - school
  - Document
  - GamePi
  - Engineering
  - RaspberryPi
icon: https://cdn.ant.lgbt/icons/gamePi.png
---

This document is the documentation of one of my school projects.
You can download the Design-Files here: <br>
**stl:** [Part1](https://cdn.ant.lgbt/files/GamePi-Part1.stl), [Part2](https://cdn.ant.lgbt/files/GamePi-Part2.stl) <br>
**online:** [OnShape](https://cad.onshape.com/documents/0c50281a8b26b3f78908ffd1/w/1d20597d1fdd82ccd1ed67c1/e/baef79fef3d09c4a0932a417), Thingiverse <br>
**documents:** [tarball](https://cdn.ant.lgbt/files/GamePi-Tablet.tar.gz)

![](https://cdn.ant.lgbt/img//GamePi-Tablet.png)

## Documentation

I didn't find it particularly difficult to choose a project. However, what I did find challenging was the implementation of this project, as I didn't have much experience designing such a complex 3D object when I started. Through this project, I learned a lot in this and other areas. There were some issues, especially towards the end, that I hadn't anticipated.

## Work Process

Since I already knew a few things about such a project at the beginning, I was able to get a rough idea of what I needed to do. Unfortunately, not everything went according to plan, and some unexpected complications arose.

I started by sketching how the project should look in the end and what functions it would have. Then I began to realize the design in OnShape (a CAD program). This was arguably the hardest step of the project, as I had very little experience with 3D modeling at that point. To expand my knowledge in this area, I created some additional models and devices in my spare time. I had to adjust this 3D model several times during the work process to meet the technical specifications. I also did a lot of research on possible software solutions and hardware components to make my project functional.

The software was another weak point. I really wanted to use RetroPie, as it is one of the few well-known options and supports a wide range of software, including various emulators, Steam streaming, Kodi, and several other Linux-ARM-compatible games and programs. Unfortunately, RetroPie is not (yet) available for the Raspberry Pi 3A+—the one I wanted to use. Therefore, I had to install it manually and compile some parts of it by hand. Needless to say, this took quite a long time. Later, however, I had to switch to a Raspberry Pi 3B+ because the software refused to work properly.

The electronics were another debacle. The joysticks were a doomed idea. Initially, I thought I could connect the joysticks via GPIO. However, I quickly realized that this would be more complicated than expected. So, I tried using the Teensy, specifically the Teensy++ 2.0. I found a project from Adafruit, which also inspired the name of this project. In that project, they used a Teensy (specifically the Teensy LC) to convert the analog joystick signals into digital ones and send them to the Pi via USB. For reasons unknown to me, this didn’t work in my project. Unfortunately, time was running out, so I decided to simply leave out the joysticks. This means that some functions, like playing "modern" games, might be difficult, but I had to find a way to finish this project. Additionally, I found some games that still work fine with a D-pad.

Throughout my work, I documented all my successes (and failures) in this project journal to share with others, hoping that the same mistakes would not be repeated.

## Reflection

I set myself goals that were too ambitious and had to hold myself back at times. Still, I learned a lot and was able to solve or work around most of the problems that arose.

If I were to start a similar project again, I would simplify some things instead of always trying to incorporate the best possible option. For example, I would focus more on the hardware than on the software and try to use less complex basics (e.g., gpionext) from the beginning. As a result, I ran out of time and resources to make the tablet battery-powered.

## Conclusion

What I liked about the project course was that I could freely decide what I wanted to do. You can implement your own ideas and also learn independent thinking.

I'm fairly certain that even outsiders with little to no experience in electronics will understand the effort involved in this project. However, those who have some knowledge will probably not be very impressed.

Despite the many problems and remaining possibilities for improvement that this project has brought to light, I consider it a success, as I learned a lot about 3D modeling and electronics. I also now have a functional tablet that I can actually use. Since the tablet is based on Linux, it will undoubtedly remain operational for a very long time.

If I were to do something like this again, I would definitely take a different approach, but I still consider the project a success, as the documentation is now almost complete. Among other things, I added the images to the gallery.

## Gallery and Schemas

![Front-Plate](https://cdn.ant.lgbt/img//GamePi-figure1.png)<br>
![Prototyp](https://cdn.ant.lgbt/img//GamePi-figure2.png)<br>
![Bootscreen am fertigen Produkt](https://cdn.ant.lgbt/img//GamePi-figure3.png)
