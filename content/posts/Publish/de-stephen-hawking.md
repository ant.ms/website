---
title: "DE: Stephen Hawking"
date: 2018-12-13T11:22:47+01:00
draft: false
toc: true
categories:
  - German
tags:
  - work
  - school
  - Document
  - Stephen Hawking
  - Science
  - German
banner: archived
---

This document is a short biography about Stephen Hawking I wrote in school.

<!--more-->

<br>

# Weshalb ich Stephen Hawking gewählt habe

Ich habe Stephen Hawking gewählt, da er viele Rückschläge erlitten hatte
und trotzdem immer weiter gemacht hat. Die meisten Menschen hätten
wahrscheinlich bereits bei der Diagnostizierung einer unheilbaren
Krankheit aufgegeben, er hat jedoch nicht aufgegeben und immer weiter
gekämpft. Zudem hat er viele Menschen für die Wissenschaft begeistert
und hat selbst die Physik mehr als einmal revolutioniert.

# Steckbrief/Lebenslauf

Stephen William Hawking wurde am 8. Januar 1942 in
Oxford, England geboren. Während des zweiten Weltkrieges zog seine
Familie nach Oxford, da dies als sicherer galt als London. Mit acht
Jahren zog er dann nach St. Albana, eine kleine Stadt etwa zwanzig
Kilometer nördlich von London. Drei Jahre später besuchte er die
St. Albans Schule und später (1959) die Universität von Oxford. Stephen
Hawking wollte Mathematik studieren entgegen den Vorstellungen seines Vaters, die in Richtung Medizin tendierten. Da kein Studium der Mathematik verfügbar war, entschied er sich für ein Studium in Richtung Physik. Drei Jahre später wurde er mit einem erstklassigen Abschluss in Naturwissenschaften ausgezeichnet.

Im Oktober 1962 begann Stephen Hawking mit der Forschung in Richtung der Kosmologie bei der Universität Cambridge. Nur Monate, kurz nach seinem 21. Geburtstag wurde bei ihm ALS diagnostiziert. Dies ist eine unheilbare Krankheit die nach und nach alle motorischen Fähigkeiten des Körpers lahmlegt. Nach vier Jahren Pionierarbeit in diesem Bereich erhielt er den “Adams Prize” für seine Arbeit über Singularitäten und die Geometrie der Raumzeit. 1968 wechselte Hawking den Fachbereich zu Astronomie, jedoch wechselte er fünf Jahre später dann wieder zurück zu DAMTP (Department of Applied Mathematics and Theoretical Physics) und arbeitete dort als Assistent. Während dieser Zeit veröffentlichte er sein erstes Buch: “The Large Scale Structure of Space-Time”. 1974 wurde er zum Mitglied von der Royal Society gewählt. Zwischen 1979 und 2009 besuchte er den Lucasischen Lehrstuhl für Mathematik.

## Sprachcomputer

Stephen Hawking’s Sprachcomputer war der DECtalk DTC01. Zu Beginn wurde der Sprachcomputer durch einen Taster bedient, damit war es möglich bis zu 15 Wörter pro Minute schreiben. Später wurde der Taster durch einen Infrarot-Sensor ausgetauscht, der die Augenposition von Stephen Hawking überwacht. Kurz gefasst kann man den Sprachcomputer mit den Augen steuern.

Die Stimme, die der Sprachcomputer ausgab, hatte einen Amerikanischen Akzent, obwohl Stephen Hawking Brite war, er nahm es mit Humor.

# Literatur

## Bücher

Stephen Hawking hat über die Jahre viele Bücher geschrieben. Er
war also nicht nur Wissenschaftler, sondern auch ein fleissiger
Autor. Hier sind seine bekanntesten Bücher aufgelistet.

**2001 - Das Universum in der Nussschale**

In diesem Buch wird auch für einen Laien verständlich erklärt,
wie das Universum aufgebaut ist. Er behandelt in diesem Buch
verschiedene Vorschläge zu wissenschaftlichen Theorien wie z.B. die
Stringtheorie und die Super-Gravitation.

2007 -2009 “**Der geheime Schlüssel zum Universum**”
und “**Die unglaubliche Reise ins Universum**”

Diese drei Kinderbücher schrieb Stephen Hawking mit
seiner Tochter Lucy Hawking. In diesen Büchern erklärt er zusammen
mit seiner Tochter die Geheimnisse des Universums in für Kinder
geschriebenen Geschichten.

**2011 - Eine kurze Geschichte der Zeit**

Ist mit Abstand sein berühmtestes und meistgelesenes Buch. Es
erklärt die Komplexität des Universums mit besonderem Fokus auf der
Zeit. Es befasst sich auch mit den existenziellen Fragen wie z.B.
“Woher kommen wir?” und “Warum ist das Universum so, wie es
ist?”.

**2013 - Meine kurze Geschichte**

Dies ist Hawking’s Autobiographie, in der er sein
komplettes Leben offenlegt.

**2018 - Kurze Antworten auf grosse Fragen**

Dies ist Stephen Hawking’s letztes Buch, es wurde erst nach
seinem Tod veröffentlicht und handelt von Hawking’s Meinung über
grosse Fragen, die die Menschheit beschäftigt. Darunter sind auch
Fragen wie: «Woher kommen wir?» und «Können wir den Weltraum
besiedeln?». Dies ist eines der Bücher, die ich selbst auch gelesen
habe und ich kann es nur empfehlen.

## TV-Auftritte

Stephen Hawking war eine sehr berühmte Persönlichkeit und hatte somit auch einige TV-Auftritte in Serien wie “Die Simpsons”, “Futurama”, “Startrek: Next Generation”, wo er gegen Data, Isaac Newton und Albert Einstein Poker spielte, und “The Big Bang Theory”. Als Stimme trat er zudem auch in weiteren Serien auf.

# Wissenschaftliche Theorien (Vereinfacht)

Disclaimer: Dies sind überaus komplexe Theorien und erfordern ein sehr gutes Fachwissen. Hier wurde versucht die Theorien für jeden verständlich zu machen.

Bereits 1960 gelang Stephen Hawking, zusammen mit Roger Penrose, der theoretische Nachweis von der Existenz von Singularitäten in der allgemeinen Relativitätstheorie. 1666 erhielt er hierfür den angesehenen “Adams Prize” der Universität Cambridge.

1974 entwickelte er die Theorie der von Schwarzen Löchern abgegebenen “Hawking-Strahlung”, nach dem Schwarze Löcher in der Quantenfeldtheorie (je nach der Masse des Schwarzen Lochs mehr oder weniger schnell) zerstrahlen. Damals versuchte er, dass 1973 von Jacob Bekenstein eingeführte Konzept der Entropie (Wendung/Umwandlung) Schwarzer Löcher mit Hilfe der Quantenmechanik (erweiterte Mechanik, die es ermöglicht, das mikrophysikalische Geschehen zu erfassen) zu verstehen. Dies war eine seiner bedeutendsten Entdeckungen.

In den 1980-1990er Jahren entwickelte Hawking mit James Hartle einen Zugang zur Quantengravitation über eine euklidische Pfadintegralformulierung (alle möglichen Pfade zwischen Punkt A und B).

Auf der “General Relativity” Konferenz in Dublin 2004 hat Stephen Hawking angekündigt, dass er das Problem des Informationsverlustes Schwarzer Löcher hat. Schwarze Löcher „verschlucken“ Materie und damit Informationen. Sie selbst werden aber nur durch wenige Parameter definiert: Ihre Temperatur und Entropie, diese sind proportional zu ihrer Oberfläche. Das Problem spielt eine wichtige Rolle in der Quantengravitation und war seither Bestandteil kontroverser Debatten. 1997 hat Hawking eine Wette abgeschlossen, dass Informationen verloren gehen, wenn sie in ein Schwarzes Loch fallen. Dies würde jedoch gegen
das Gesetz der Thermodynamik verstossen. Hawking änderte hiermit seine Meinung, er verlor die Wette.

Hawking’s letzte wissenschaftliche Arbeit, die erst wenige Tage vor seinem Tod fertiggestellt wurde, wird als grossen Schritt in die Richtung einer Lösung des Problems des Informationsverlustes in Schwarzen Löchern angesehen.

# Schlusswort

Zum Schluss möchte ich noch sagen, dass ich Stephen Hawking sehr bewundere. Obwohl er sehr berühmt und genial war blieb er immer bescheiden und nutzte die Popularität um die Menschheit zu warnen und um Menschen zu helfen. Er hat nicht nur die Wissenschaft revolutioniert sondern hat auch, gerade wegen seiner Erkrankung an ALS, vielen Menschen Hoffnung gemacht.
