---
title: "What I learned from keeping a diary"
date: 2021-08-02T18:10:47+02:00
draft: false
categories:
  - Reflection
tags:
  - Life
---

Writing a diary can seem really tedious and counter-productive, but it has some real benefits. To be clear, this does **not** mean, that you have to write down every single thing that happens in every day. It's here to write down some of the most important memories.

It can be great to have the ability to **look back** at past events and see how you reacted there, or to see what your arguments for a certain important decision were. Especially by adding images, videos, and other elements to your diary, you add a bit of immersion to your memories, and help to protect them for longer.

Some might think, that Social Media (especially Twitter, Instagram, ...) already do this for you, as some people post almost everything to those platforms... but that is exactly the problem. By posting everything, even the not very important things, it becomes pretty much impossible to search for something specific. In addition to that, I would argue that there are always things, which you'd rather not publish on there, and these things especially are often the things worth preserving. Of course, a diary is only advantageous, if you can **find things**. Having a good search tool for my diary, is one of the most important things.

It's also important to note, that this is also a good **retrospective** of your day. So you consciously think about the things you did. This helps not to do mistakes again and again and again.

Finally, I can't not mention [Jrnl](/posts/dev/jrnl/). Which is my solution to this problem. By using an easily accessible SQL Database, you can also transfer data in and out easily, which I think is quite important, for something you better don't want to have to copy-paste to a new piece of software (if/when you switch to one) manually.
