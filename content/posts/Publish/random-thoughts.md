---
title: "Random Thoughts"
date: 2020-04-05T13:35:41+02:00
draft: false
categories:
tags:
banner: archived
---

In a void, emotions are static. In us, lies the potential for growth, as well as fall. Look beyond the goal, to see the path.
