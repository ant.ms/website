---
title: "3D-Modell: LED-Cube"
date: 2018-12-24T13:22:39+01:00
draft: false
categories:
  - Object
tags:
  - LED
  - Thingiverse
  - STL
  - 3D-Printing
  - Christmas
  - Gift
---

This is an LED-Cube that I created as a Christmas gift for my family.

<!--more-->

![](https://cdn.ant.lgbt/img//LED-Cube1.jpg)

It consists of an cheap Circuit-Board responsible for charging, a battery, a switch and an LED. So its a pretty primitive construction.

I modeled this Cube in OnShape in order to get some training in semi-professional modeling. The files are free to use, modify and publish so do whatever you want with them.

Download:

> **stl:** <a href="https://cdn.ant.lgbt/files/LED-Cube - Part 1.stl">Part 1</a>, <a href="https://cdn.ant.lgbt/files/LED-Cube - Part 2.stl">Part 2</a><br> > **online:** [Thingiverse](https://www.thingiverse.com/thing:3330761), [OnShape](https://cad.onshape.com/documents/aee41ad727ff9fbcbd3ab7c0/w/7ed4e287fb53366b5ed5a2fa/e/b2e8e82c164ab686fce6cdc0)
