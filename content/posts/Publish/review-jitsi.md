---
title: "Review: Jitsi"
date: 2020-04-01T15:02:47+02:00
draft: false
banner: archived
categories:
  - Review
tags:
  - Jitsi
  - talk
icon: https://cdn.ant.lgbt/icons/jitsi.jpg
---

I've been using Jitsi for a while now and I can't say enough good things about it. For everyone that doesn't know: Jitsi is a (if you want self-hosted) video conference-/call software that's 100% FOSS. In my experience it has the best quality of any I've tried, which resulted in me using it for the last month or so every single day (with about two exceptions :( ). It's not perfect though, I don't like the fact that it has a watermark on the default client (which you can remove probably but I haven't figured out how) but the fact that it's p2p is AMAZING!!!

> Check out there website: [jitsi.org](https://jitsi.org/) or directly start a call from your browser (doesn't work well for me though) here: [meet.jit.si](https://meet.jit.si/)
