---
title: "Migrating my network over to ZeroTier"
date: 2021-09-09T17:02:47+02:00
draft: false
categories:
  - Review
tags:
  - Network
  - talk
icon: icons/zerotier.png
banner: archived
---

> ZeroTier creates secure networks between on-premise, cloud, desktop, and mobile devices.

This is how ZeroTier explains their software, and it is kind a unjust explanation, for such a magical piece of networking magic. I would explain it like this: Zerotier functions as a "virtual LAN" (as an actual virtual network adapter), allowing all my devices (including servers ofc), to communicate with each other. You could compare it with a VPN, but only routing what it needs to, and being P2P.

For linux, there is a simple command install, which handles everything. For my mac, I needed to download an installed, which installs a "menubar-item", which functions as the GUI. On FreeBSD, you apparently need to compile it yourself, but I have found, that there is already a package in the repo (no clue why they don't mention that). I have found the FreeBSD package to be quite unreliable for some reason, so I might compile it from scratch after all.

The only problem I have, is that you have to realistically host the controllers yourself, if you want to have more than 50 devices. This is because their subscriptions are incredibly expensive, their lowest tier (after free), is 50$ per month, and you don't even get much more out of it.

In a nutshell, this saves me a lot of trouble, and increases the security of my entire infrastructure (as I don't need to have most ports open anymore).
