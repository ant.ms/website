---
title: "My opinion on MacOS (on the M1)"
date: 2021-04-27T13:42:47+02:00
draft: false
categories:
  - Review
  - Reflection
tags:
  - MacOS
  - Apple
icon: https://cdn.ant.lgbt/icons/apple.png
---

A while ago (around two months ago), I decided to try out a MacBook. I know that everyone that knows me would scream at me, as I have always been hating Apple for what I do, but I never had any proof for that hate... until now. So when Apple announced the M1 processor, I though I'd give it a go, and tried it as my main PC (I use my Laptop docked as my only PC).

**Positive things**

- Most of my Development tools worked surprisingly well (even Docker works quite well... even though it like 2GB more than under Linux!?!?!).
- The GUI is quite similar to a mix between KDE and mostly GNOME, so I was quickly able to get used to it.
- The Touchpad is great, like really great.
- CLIENT SIDE DECORATIONS ARE GREAT (aka: The Window controls being inside the Window, instead of having a titlebar).
- The MacBook has a really quick wake-up feature. It is usually turned on (from sleep) by the time I finished lifting the screen.

**Negative things**

- Not having a USB-A port is not a problem for me, but what has been a problem for me, is that both (you can't get more) of the USB ports are at the same side of the laptop. This causes me to not be able to have a dongle and a usb-stick plugged in at the same time.
- Software support for packages (like from brew and similar) is limited at best. Rosetta seems to not be able to work with these kind of utils
- Applications are fast and responsive... once they launched. But launching a simple web browser can take around 30 Seconds at times
- Memory runs out way too quickly, and when it does, MacOS does not seem to free it up a fast enough, which means that everything starts to lag
- Installing another OS (like Linux is currently not possible)
- The AppStore is mostly useless and quite buggy.
- Two our of my three Network Adapters (which both work seamlessly under Linux, Windows, Android, and even my TV) don't work.

At the end of the day: It was a great experiment, but I already ordered a Laptop to put Linux on to get pretty much everything fixed which I miss with the Mac. I'm glad I did this experiment though, as MacOS has a lot of cool (and a lot of annoying) features or kinks, which neither Windows or Linux usually has.
