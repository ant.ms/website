---
title: "Program: GifAnt (JS)"
date: 2020-05-01T12:40:41+02:00
draft: false
categories:
  - Web
tags:
  - JS
  - HTML
  - CSS
  - Software
  - Multimedia
icon: https://cdn.ant.lgbt/icons/gifAnt.png
gitlab: https://git.ant.lgbt/ConfusedAnt/gif-ant
demo: https://gif.ant.lgbt/
---

> Disclaimer: I do NOT own the copyright to these gifs, they're just in there in order to function as an example to what this App is capable of.

![](https://cdn.ant.lgbt/img//gifAnt.jpg)

GifAnt is a small web-based "Gif-Picker", it shows you a list of available gifs (which you can define in the data file) that you can sort based on tags. It allows you to copy the link of the gif by clicking on it (tested with Firefox and Chrome, some browsers might not work).
