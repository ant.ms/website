---
title: "Blogging Duck"
date: 2023-01-16
draft: false
categories:
  - School
  - Web
icon: icons/blogging_duck.png
gitlab: https://gitlab.com/mygibz/m183-project
---

Blogging Duck is a Blog-Application that I created at school for the M183 module. We quite a few requirements to fulfill, so the End-Goal was quite stiff from the beginning. It features the ability to make posts (shocking, I know), write comments, register and login (through Github or username + password + phone 2FA), and much more.

![Blogging Duck front page](/assets/images/posts/blogging_duck.png)

I've had a lot of fun trying out new things in the design front. I rarely design light mode applications, so this teached me a lot about how to try to make light mode look good. I also strove away from using my usual "shadow based" light mode design to a much flatter design choice, and I must say that I think it ended up alright. Clearly this is not a super polished look, but I wanted to make it look both nice and also looking "functional" (aka: not too fancy, but also not plain HTML), and I think I succeeded in that.

From the technology perspective this project is nothing impressive. It's based around [SvelteKit](https://kit.svelte.dev/) for both the frontend and the backend, as well as [Prisma](https://www.prisma.io/) (using the sqlite adapter) for the Database.

If you are interested in more details, please look at the [README.md](https://gitlab.com/mygibz/m183-project/-/blob/main/README.md) file in the project repository for a lot more information about this project.
