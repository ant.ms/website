---
title: " Simple GTK Text Editor (Vala)"
date: 2019-12-26T20:31:54+01:00
draft: false
categories:
  - Desktop
  - GTK
tags:
  - C
  - Vala
  - Software
  - GTK
icon: https://cdn.ant.lgbt/icons/file-edit.png
gitlab: https://gitlab.com/ant.ms/texteditor
banner: archived
---

This is a very simple text editor I wrote in Vala for GTK. It doesn't have many features, but it can load and save files so you can edit them however you want, I hope you'll have fun with it.

![](https://cdn.ant.lgbt/img//simpleTextEditor.png)

This is merely just a proof of concept, I might (probably) make another text-editor in the future.
