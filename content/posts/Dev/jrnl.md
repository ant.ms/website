---
title: " Jrnl (PHP)"
date: 2020-08-13T12:18:00+02:00
draft: false
categories:
  - Web
tags:
  - jrnl
  - HTML
  - PHP
  - CSS
  - Software
icon: icons/jrnlWeb.png
gitlab: https://gitlab.com/ant.ms/jrnl
---

The inspiration for this application goes quite a bit back. There is a diary software for the Linux terminal called [Jrnl](https://jrnl.sh/). It's a (not well known but certainly not unknown) but very well working application. In order to make things easier for me, I made a web-interface for it. The entire App is password protected in order to make sure that no-one can access your personal thoughts. Over time that web-interface developed and then turned into this application. It no longer uses [Jrnl](https://jrnl.sh/) as a backend and does far more than just keeping simple diary entries.

If you have any thought in this piece of software or would like to see a change or a new feature, don't hesitate to message me under [confused@ant.lgbt](mailto:confused@ant.lgbt) or any other of my communication channels. This project means a lot to me, and is one of the few pieces of software on my website, that I actively want to develop for a very long time.

![](https://cdn.ant.lgbt/img//jrnl2Web.png)

Jrnl-Web has emoji-support aswell, you can enable it by typing e.g. `​:​joy​:​` which will be displayed as the correct emoji (emojis in this applications are rendered using [emoji.js](https://github.com/iamcal/js-emoji), hosted on my own CDN. You can selfhost that though, if you prefer that).

# Normal Entries

<img src="https://cdn.ant.lgbt/img//jrnl2add.png" style="float:right;height:12rem;margin-left:14px;margin-top:-2em" alt="mobile Optimisation Jrnl Web"/>

Jrnl-Web can't only display posts but also create them. By clicking on the Add-Button, you're presented with a popup that allows you to write your entry. You can enter the title of the post. And then start writing your entry, also note that you can use emoji, in order to make it easier for you to bring your thoughts and feelings to paper ~~(or rather bits)~~.

<div style="clear:both"></div>

# Special Days

<img src="https://cdn.ant.lgbt/img//jrnl2special.png" style="float:left;height:16rem;margin-right:14px" alt="mobile Optimisation Jrnl Web"/>

Special days is a new feature that I came up with. Sometimes you have days that are important to you. Those may be days, that you spend with your loved ones, or just an extra-ordinary day in general. This feature allows you to (in addition to regular entries) add an image and a detailed description of the day.

I personnally use this feature to detail how the day was. Like what I was doing and what happened. And then write my subjective feelings about it in a regular post. This is especially useful, when you're using the multi-user functionality of this application.

<div style="clear:both"></div>

# Mobile Optimisation

<img src="https://cdn.ant.lgbt/img//jrnl2Mobile.jpg" style="float:right;height:12rem;margin-left:10px" alt="mobile Optimisation Jrnl Web"/>

This application has great mobile support and works well on every display size imaginable. The UI elements where chosen to be compatible with various input types and where then optimized to look good on any display.

If you use the default dark theme, the color of the status-bar on your phone will even match the design of the rest of the application and will give the bar a 'floating' effect (this, of course, only works when installing the app as a pwa).

<div style="clear:both"></div>

# Customize

![Settings-bar](https://cdn.ant.lgbt/img//jrnl2Setings.png)

In the settings screen, you can change the accent color, the way that the headers (post-titles) are displayed, if you want the bar to be on the top or the bottom of the screen (which can be very useful on mobile devices) and you can select what theme you want to use.

<div style="clear:both"></div>

# Emoji Support

You can use emoji's, just by typing the emoji directly, or by using one of these shortcodes:

| Shortcode                                                                                                | Emoji |
| -------------------------------------------------------------------------------------------------------- | ----- |
| :​joy​:                                                                                                  | 😂    |
| :​sob​:                                                                                                  | 😭    |
| :​sweat_smile​:                                                                                          | 😅    |
| :​smiling_face_with_hearts​: <br> :​smiling_face_with_3_hearts​: <br> :​smiling_face_with_three_hearts​: | 🥰    |

# Secure and lightweight

JrnlWeb uses PHP on the server, in order to make the app as fast and lightweight on the client side as possible. In addition, it uses [PHP Sessions](https://www.w3schools.com/php/php_sessions.asp) for it's login system.

# Search Function

Jrnl has a search function, since the latest update. It allows you to search through regular posts, by text in the title or the content.

# Plans for the future

- Pinnable posts
- Ability to edit and delete entries
- and much more

# Host it yourself

Installing it couldn't be simpler, just execute this command, which will create a docker container with all the dependencies:

`sudo docker run -d -v $(pwd)/jrnl:/var/www/html/data -p 9090:80 --name=jrnlweb confusedant/jrnlweb:latest`
