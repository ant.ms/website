---
title: "Program: Project Julisite (HTML)"
date: 2020-08-10T12:50:41+02:00
draft: false
categories:
  - Web
  - Project
tags:
  - Arduino
  - Project
  - HTML
  - CSS
  - JS
  - Javascript
  - Software
  - Cutie
icon: https://cdn.ant.lgbt/icons/leo.jpg
gitlab: https://git.ant.lgbt/ConfusedAnt/julisite
demo: https://julienacker.dev/
---

This project started off a bit different than most of my projects do. My boyfriend mentionend, that he has a "website" (it's not... feature-full) a while ago. Upon seeing that I thought that I could make him a new one, using my [design-language](/posts/development/antdesign/).

![Screenshot of Julisite](https://cdn.ant.lgbt/img//julisite.png)

I'm really happy with how it turned out, and I'm looking forward to seeing it being used.
