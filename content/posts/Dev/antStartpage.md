---
title: "Startpage"
date: 2021-05-25T08:40:41+02:00
draft: false
categories:
  - Web
  - TypeScript
  - SCSS
tags:
  - TypeScript
  - SCSS
  - HTML
  - CSS
  - Software
  - Useful
icon: icons/startpage.png
---

This start page is made to be able to function as a "New Tab" page in your browser (through extensions like [Custom New Tab Page](https://github.com/methodgrab/firefox-custom-new-tab-page)). You can customize it any way you want. The app supports dark mode by reacting to your systems setting.

![](https://cdn.ant.lgbt/img//startpage.png)

First run `npm install` to install all the dependencies for this project. Edit _src/ts/config.json_ to fit your needs, after that you can compile it with `npm run build`, or just run it locally (useful for development) with `npm run dev`. If you like this project, you can hop over to it's [Reddit post](https://www.reddit.com/r/startpages/comments/nefjea/i_got_inspired_by_people_on_this_subreddit_and/) to give feedback.

> Check live version [antexperiments.gitlab.io/webapps/startpage](https://antexperiments.gitlab.io/webapps/startpage/)<br>
> Or check out the code on [Gitlab](https://gitlab.com/AntExperiments/webapps/startpage), feel free to fork :)
