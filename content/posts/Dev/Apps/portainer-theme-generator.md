---
title: "Interactive: Portainer Theme Generator"
date: 2021-09-12T13:20:00+02:00
draft: false
categories:
  - Tools
tags:
  - Software
  - Live
  - Tools
  - Theme
# icon: https://cdn.ant.lgbt/icons/ph.png
banner: archived
---

**How to use?**<br>
Select a color down below, then copy the code generated below and use a custom CSS plugin (like [Custom Style Script](https://chrome.google.com/webstore/detail/custom-style-script/ecjfaoeopefafjpdgnfcjnhinpbldjij))

<div style="display:flex;justify-content:space-between;align-items:center">
<label>
    <input type="color" id="yourColor" value="#46a385">
    <span>Select Color</span>
</label>

<!-- <input type="text" id="yourImage" placeholder="Image url (visible on start page)" style="width: 20em"> -->
</div>

<div class="highlight">
    <pre class="chroma">
        <code id="yourOutput" class="language-css" data-lang="css">
            <!-- To Be Filled in by JS -->
        </code>
    </pre>
</div>

<script>
    const LightenDarkenColor = (color, amt) => {
        var usePound = false;
        if ( color[0] == "#" ) {
            color = color.slice(1);
            usePound = true;
        }

        var num = parseInt(color, 16);

        var r = (num >> 16) + amt;

        if ( r > 255 ) r = 255;
        else if  (r < 0) r = 0;

        var b = ((num >> 8) & 0x00FF) + amt;

        if ( b > 255 ) b = 255;
        else if  (b < 0) b = 0;

        var g = (num & 0x0000FF) + amt;

        if ( g > 255 ) g = 255;
        else if  ( g < 0 ) g = 0;

        return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
    }

    const updateColor = () => {
        const accentColor = document.querySelector("#yourColor").value
        const lightAccent = LightenDarkenColor(accentColor, -10)
        document.querySelector("#yourOutput").innerText = `
#sidebar-wrapper, .sidebar-header, .boxselector_wrapper input[type=radio]:checked+label
{ background: ${accentColor} }

a, .fa.blue-icon, .fab.blue-icon, .btn-link, .cm-s-default .cm-atom,
.boxselector_wrapper input[type=radio]:checked+label:after, a:focus, a:hover
{ color: ${lightAccent} }

ul.sidebar .sidebar-list a.active, ul.sidebar .sidebar-list a:hover,
.btn-primary, .widget .widget-icon, .label-primary
{ background: ${lightAccent} !important }

.boxselector_wrapper input[type=radio]:checked+label,
.boxselector_wrapper input[type=radio]:checked+label:after
{ border-color: ${lightAccent} }

ul.sidebar .sidebar-list a, ul.sidebar .sidebar-title
{ color: #ffffffe0 }

ul.sidebar .sidebar-list a:hover { border-left: 3px solid ${LightenDarkenColor(accentColor, 30)} }

.bootbox-form .checkbox :checked~i, .switch :checked+i
{ box-shadow: inset 0 0 1px rgb(0 0 0 / 50%), inset 0 0 40px ${accentColor} }`
    }

    document.querySelector("#yourColor").addEventListener('change', updateColor)
    updateColor()
</script>

## Example (#a44650)

<div class="grid-2">

![](https://i.ibb.co/9VHH8jy/Web-capture-12-9-2021-135717-app-ant-lgbt.jpg)

![](https://i.ibb.co/QMwY5f3/Web-capture-12-9-2021-13576-app-ant-lgbt.jpg)

</div>
