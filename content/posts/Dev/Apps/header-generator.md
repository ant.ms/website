---
title: "Interactive: Header-Generator"
date: 2020-05-26T16:24:00+02:00
draft: false
categories:
  - Tools
tags:
  - Software
  - Live
  - Tools
icon: https://cdn.ant.lgbt/icons/html.png
---

<table style="table-layout: fixed;">
    <tr>
        <td class="floatOnMobile">
            <table><tr>
                <td><i class="fas fa-wrench"></i> Include Dependencies</td>
                <td>
                    <div class="onoffswitch">
                        <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="YourDepenencies" tabindex="0" onchange="updateOutput()">
                        <label class="onoffswitch-label" for="YourDepenencies">
                            <span class="onoffswitch-inner"></span><span class="onoffswitch-switch"></span>
                        </label>
                    </div>
                </td>
            </tr></table>
        </td>
        <td class="floatOnMobile">
            <table><tr>
                <td><i class="fa fa-comment"></i> Comment style</td>
                <td>
                    <select id="YourLanguage" onchange="updateOutput()">
                        <option value="HTML">&lt;!-- gay --&gt;</option>
                        <option value="CSS">/* gay */</option>
                        <option value="C" selected>// gay</option>
                        <option value="C+">/// gay</option>
                        <option value="bash"># gay</option>
                    </select>
                </td>
            </tr></table>
        </td>
    </tr>
    <tr>
        <td class="floatOnMobile">
            <table><tr>
                <td>
                    <label for="YourProject"><i class="fas fa-box-open"></i> Projekt Name</label>
                    <input type="text" id="YourProject" placeholder="Amazing Application..." oninput="updateOutput()">
                </td>
            </tr></table>
        </td>
        <td class="floatOnMobile" rowspan="5">
            <table><tr><td><i class="fa fa-comment-alt"></i> Description</td><tr>
                <td>
                    <textarea id="YourDescription" cols="30" rows="10" oninput="updateOutput()" placeholder="This is an Application that..."></textarea>
                </td>
            </tr></table>
        </td>
    </tr>
    <tr>
        <td class="floatOnMobile">
            <table><tr>
                <td>
                    <label for="YourLink"><i class="fas fa-link"></i> Link to Repo</label>
                    <input type="url" id="YourLink" placeholder="https://gitlab.com/ant.ms/website" oninput="updateOutput()">
                </td>
            </tr></table>
        </td>
    </tr>
    <tr>
        <td class="floatOnMobile">
            <table><tr>
                <td>
                    <label for="YourName"><i class="fas fa-user"></i> Name</label>
                    <input type="text" id="YourName" placeholder="John Doe..." oninput="updateOutput()">
                </td>
            </tr></table>
        </td>
    </tr>
    <tr>
        <td class="floatOnMobile">
            <table><tr>
                <td>
                    <label for="YourEmail"><i class="fas fa-envelope"></i> Email</label>
                    <input type="email" id="YourEmail" placeholder="Your Email..." oninput="updateOutput()">
                </td>
            </tr></table>
        </td>
    </tr>
    <tr>
        <td class="floatOnMobile">
            <table><tr>
                <td>
                    <label for="YourCommand"><i class="fas fa-terminal"></i> Build-Command</label>
                    <input type="text" id="YourCommand" placeholder="rm ./app; valac app..." oninput="updateOutput()">
                </td>
            </tr></table>
        </td>
    </tr>
</table>

<br>

**Output**

<div class="highlight">
    <pre class="chroma">
        <code id="YourOutput" class="language-html" data-lang="html">
            <!-- To Be Filled in by JS -->
        </code>
    </pre>
</div>

<style>
    /* TODO: Make external */
    td { border: 0px }
    table th, table td { padding: 1%; border: 0px; }
    input[type=text], input[type=email], input[type=url], .onoffswitch, select { float: right }
    input[type=text], input[type=email], input[type=url] { width: 55%; }
    #git-logo:before { content: "\f296" !important; font-family: "Font Awesome 5 Brands"; }
    #git-logo:after { content: "\f09b" !important; font-family: "Font Awesome 5 Brands"; }
</style>

<script>
    function updateOutput() {
        // #region set Variables from Variables

        switch (YourLanguage.value) {
            case 'HTML':
                var multiline = true;
                var start = "\n<!--";
                var end = "-->";
                var line = "\n\t";
                break;
            case 'CSS':
                var multiline = true;
                var start = "\n/*";
                var end = "*/";
                var line = "\n\t";
                break;
            case 'C':
                var multiline = false;
                var line = "\n// ";
                break;
            case 'C+':
                var multiline = false;
                var line = "\n/// ";
                break;
                break;
            case 'bash':
                var multiline = false;
                var line = "\n# ";
                break;
        }

        // #endregion set Variables from Variables

        if (multiline)
            var output = start;
        else
            var output = "";

        if (YourProject.value != "")
            output += line + "Program:        " + YourProject.value;

        output += line + "Version:        0.0.1";

        if (YourDescription.value != "") {
            output += line + "Description:    ";
            // YourDescription.value.replace("\n", line + "                ")
            if (YourDescription.value.length <= 72 - (line.length + 16))
                output += YourDescription.value;
            else
                output += (YourDescription.value + " ").replace(/([^\n]{1,72})\s/g, line + '                $1').replace(line + '                ', "", 1);
        }


        if (YourName.value != "")
            if (YourEmail.value != "")
                output += line + line + "Author:         " + YourName.value + " (" + YourEmail.value + ")";
            else
                output += line + line + "Author:         " + YourName.value;

        if (YourName.value == "" && YourEmail.value != "")
                output += line + "Email: " + YourEmail.value;

        if (YourLink.value != "")
            output += line + "Repository:     " + YourLink.value;

        if (YourCommand.value != "")
            output += line + "Build-Command:  " + YourCommand.value;

        output += line + line + "Last Change:    " + (new Date);

        if (YourDepenencies.checked) {
            output += line + line + "Dependencies (Required): ";
            output += line + "- none";
            output += line + "Dependencies (Optional): ";
            output += line + "- none";
            output += line + "Precondition: ";
            output += line + "- none";
            output += line + "Postcondition: ";
            output += line + "- none";
        }

        output += line + line + "Copyright (c) " + (new Date).getFullYear();
        if (YourName.value != "") {
            output += " by " + YourName.value;
        }

        document.getElementById("YourOutput").innerText = (multiline) ? output + "\n" + end: output + "\n";
    }
    updateOutput();
</script>

<!--
	Program: Ant Project
	Author: Yanik Ammann (confused@ant.lgbt)
	Repository: https://gitlab.com/ant.ms/website

	Version: 0.0.1
	Last Change: Tue May 26 2020 19:08:58 GMT+0200 (Central European Summer Time)

	Dependencies (Required):
	-
	Dependencies (Optional):
	-

	Copyright (c) 2020 by Yanik Ammann
-->
