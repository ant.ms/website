---
title: "Perle Reader"
date: 2021-06-07T14:11:00+02:00
draft: false
categories:
  - School
  - Web
tags:
icon: icons/perle-reader.png
gitlab: https://gitlab.com/mygibz/306/perle-reader
demo: https://app.ant.lgbt/reader/
---

A modern E-Book reader app, written in NodeJS.

We created this web-app in a group of three people (although more than 90% of the code is from me), with the main goal being time- and project-management. Our choice to use NodeJS for this wasn't bad, but the way we ended up using it is more than suboptimal. We pretty much ended up using NodeJS to assemble a HTML page out of "strings" on the server and then send those to the client.

Next time I would use something like React, Svelte, or something similar to that, which will access an API. Node would then only serve the base application (if at all), and act as an API for everything else

## Getting Started

1. Go into folder
2. Install Dependencies
   - Either Automatic
     1. Run `npm install`
   - Or Manually
     1. Run `npm install cookies dateformat lit-ntml markdown md5 node-sass parse-md`
3. Either
   - (if opened in VS-Code) Press the run button in your IDE
   - Run `node script.js`

## Screenshots

![](https://gitlab.com/mygibz/306/perle-reader/-/raw/main/.screenshot1.png)

![](https://gitlab.com/mygibz/306/perle-reader/-/raw/main/.screenshot2.png)
