---
title: " AntReader (PHP)"
date: 2020-03-16T16:00:41+01:00
draft: false
categories:
  - Web
tags:
  - PHP
  - HTML
  - CSS
  - Software
icon: https://cdn.ant.lgbt/icons/antReader.png
gitlab: https://gitlab.com/ant.ms/ant-reader
---

AntReader is a simple ebook reader I wrote in PHP. It's design is mainly optimized for mobile but also works quite well on desktop.

![](https://cdn.ant.lgbt/img//antReader.png)

A very specific file-structure is required for it to function:

```
_books
├── Animal Farm
│   ├── content.md
│   └── cover.png
└── The War of the Worlds
    ├── content.md
    └── cover.png
```
