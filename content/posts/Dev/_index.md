---
title: Development
date: 2021-04-25
publishdate: 2021-04-25
archived: true
titleIcon: code
---

As an Application Developer, I should be considered to be able to code, and indeed that is correct. Here you see my recent projects or experiments. Keep in mind though, that these may or may not be the most high-effort.

<div class="floats">
{{< minifloat page="/posts/dev/games" >}}
{{< minifloat page="/posts/dev/apps" >}}
</div>

<!-- TODO: Add Best projects site -->
