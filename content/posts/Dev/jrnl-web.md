---
title: "JrnlWeb (PHP)"
date: 2020-07-30T13:30:00+02:00
draft: false
categories:
  - Web
tags:
  - jrnl
  - HTML
  - PHP
  - Http
  - Software
banner: archived
bannerArchived: /posts/dev/jrnl
icon: https://cdn.ant.lgbt/icons/jrnlWebBeta.png
archived: true
---

[Jrnl](https://jrnl.sh/) is a (not well known but certainly not unknown) diary software for the Linux terminal, in order to make things easier for me, I made a web-interface for it. The entire App is password protected in order to make sure that no-one can access your personal thoughts.

If you have any thought in this piece of software or would like to see a change or a new feature, don't hesitate to message me under [confused@ant.lgbt](mailto:confused@ant.lgbt) or any other of my communication channels.

Jrnl-Web can view posts made on a certain date. The individual entries on that date are displayed as a card on the start-page. You can also enable **dark-mode** by clicking on the settings icon.

![](https://cdn.ant.lgbt/img//jrnlWeb.png)

Jrnl-Web has emoji-support aswell, you can enable it by typing e.g. `​:​joy​:​` which will be displayed as the correct emoji (emojis in this applications are rendered using [emoji.js](https://github.com/iamcal/js-emoji), hosted on my own CDN. If want you can selfhost that though).

## Choose date to view

<img src="https://cdn.ant.lgbt/img//jrnlWeb-DatePicker.png" style="float:right;height:14rem;margin-left:14px" alt="Jrnl Web Date Picker"/>

You can choose what date you want to view by either opening the sidebar (with me hamburger-menu on the top left, or by swiping from the left of the screen on mobile) or by clicking the Date-Picker button. This opens a picker that has every day highlighted and clickable where an entry is existant.

<div style="clear:both"></div>

## Mobile Optimisation

<img src="https://cdn.ant.lgbt/img//jrnlWeb.jpg" style="float:left;height:6rem;margin-right:10px" alt="mobile Optimisation Jrnl Web"/>

This application has great mobile optimisation. Thanks to the [Materializecss-Framework](https://materializecss.com/) and some custom CSS, the UI adapts to basically every screen-size. You can also open the sidebar by swiping from the left edge of the screen.

<div style="clear:both;height:1.5em"></div>

# Create Entries

<img src="https://cdn.ant.lgbt/img//jrnlWeb-AddEntry.png" style="float:right;height:14rem;margin-left:14px;margin-top:-2em" alt="mobile Optimisation Jrnl Web"/>

Jrnl-Web can't only display posts but also create them. By clicking on the Add-Button, you're presented with a popup that allows you to write your entry. You can select the date of the post (this will be calculated with a time-offset) by selecting _Today_, _Yesterday_ or _Day before yesterday_. Then enter the title of the post (that will be the first line of the post). And then you can start writing your entry, also note that you can use emoji, in order to make it easier for you to bring your thoughts and feelings to paper ~~(or rather bits)~~.

<div style="clear:both"></div>

## Customize

<img src="https://cdn.ant.lgbt/img//jrnlWeb-Settings.png" style="float:left;height:14rem;margin-right:14px" alt="mobile Optimisation Jrnl Web"/>

You can change a lot of things about this application once it's installed. You can change how the headers are displayed and the color theme that the entire Application adapts to (more things planned for the future).

In this screen you can also change your password and username.

<div style="clear:both;height:1.5em"></div>

# Plans for the future

- Search function
- Pinnable posts
- Ability to edit and delete entries

<hr>

# Host it yourself

Installing it couldn't be simpler, just execute this command, which will create a docker container with all the dependencies:

`sudo docker run -d -v $(pwd)/jrnl:/var/www/html/data -p 9090:80 --name=jrnlweb confusedant/jrnlweb:latest`
