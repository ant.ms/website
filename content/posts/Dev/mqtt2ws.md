---
title: "MQTT to Websocket bridge"
date: 2021-09-01T14:11:00+02:00
draft: false
categories:
  - Web
tags:
  - Network
  - Data
icon: icons/mqtt2ws.png
gitlab: https://gitlab.com/AntExperiments/server-stats/mqtt-to-websocket
---

This is a project, allows the visitors of my website to see the usage of my servers in real time. It is the fist of my projects to use websocket, allowing it to be very efficient and scalable.

It contains the following applications/components:

1. Server-side script
   - This raw data can already be used in e.g. Home Assistant
2. MQTT to WS bridge
   - Basically works by having two independent processes running parallel
     - A "chat" framework, which relays received messages to every connected user
     - The MQTT logic, which sends a message to the chat framework to update
3. Website to receive and display data
   - Keeping a connection open, and then updating the text once a message has been received
