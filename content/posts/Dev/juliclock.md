---
title: "Juliclock"
date: 2022-01-23T23:00:00+01:00
draft: false
categories:
  - Project
  - Object
tags:
  - Arduino
  - Project
  - C
  - ESP32
  - Software
  - Hardware
  - Cutie
icon: icons/juliclock.png
demo: https://cad.onshape.com/documents/2a0b7287f6cd4044014fb34b/w/ec6829b8e0f84175d3c83683/e/7c00f2678729979bb855a0e9
---

My while looking for potential gift ideas, I've got the idea of making a clock (as a Swiss, it just has to be natural to do this). How I would be making the clock though, would be challenging to figure out.

<div class="grid-2">
<img src="https://i.ibb.co/p1qjgLm/20220125-111129.jpg" alt="front side view of the clock" style="height: 25em; cursor: pointer; object-fit: cover" onclick="showImageToast('https://i.ibb.co/p1qjgLm/20220125-111129.jpg')"/>

<div>

Choosing the base for the clock was the first challenge. Obviously I didn't want to make the entire thing out of plastic. And it shouldn't be wasting any precious desk space. I ended up landing on this headphone stand, which looked very difficult to work with, but also was quite aesthetically pleasing. I decided on a headphone stand, which was painful to model (so, that the clock would fit snugly).

The entire thing was modelled in [OnShape](https://www.onshape.com/en/), and printing using my [Prusa Mini](https://www.prusa3d.com/category/original-prusa-mini/). It uses two cheap servos to move the clock hands, and a ESP32 to control it.

</div>
</div>
