---
title: " AntChat (PHP)"
date: 2020-03-25T12:00:41+01:00
draft: false
categories:
  - Web
tags:
  - PHP
  - HTML
  - CSS
  - Software
icon: icons/antChat.png
gitlab: https://gitlab.com/ant.ms/ant-chat
---

Ant Chat is a "as simple as possible" Web-Based (PHP) chat-application. It kinda started as a joke but I reallly enjoyed working on it.

![](https://cdn.ant.lgbt/img/antChat.png)
