---
title: "Crypt-Tool"
date: 2021-10-22T17:45:00+02:00
draft: false
categories:
  - GTK
  - Web
tags:
  - Svelte
  - Crypt
icon: icons/crypt-tool.png
gitlab: https://gitlab.com/AntExperiments/webapps/cryptic-ant
demo: https://antexperiments.gitlab.io/webapps/cryptic-ant/
---

CrypticAnt is an application, that has the goal of making conversions between encoded/encrypted text.

![](https://user-content.gitlab-static.net/19ef6e9f35bbb7a2d098729596ad09cde1989ecc/68747470733a2f2f692e6962622e636f2f6e3779336d68582f53637265656e73686f742d323032312d30372d32332d61742d31312d31302d31392e706e67)

It is build around the idea, of working similarly to Google Translate. You can select the data type, and then type in either of the textareas to convert between the two.

The reason this project exists, is because a friend of mine made a similar tool in PHP, and I thought I could do better :)

> The application can be accessed here: [antexperiments.gitlab.io/webapps/cryptic-ant](https://antexperiments.gitlab.io/webapps/cryptic-ant/)
