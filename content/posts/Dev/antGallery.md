---
title: "AntGallery (PHP)"
date: 2020-03-31T15:00:41+01:00
draft: false
categories:
  - Web
tags:
  - PHP
  - HTML
  - CSS
  - Software
  - Material
  - Materializecss
icon: https://cdn.ant.lgbt/icons/antGallery.png
gitlab: https://gitlab.com/ant.ms/ant-gallery
---

A simple Gallery APP, written in PHP which allows you to upload files. It's really simple, litteraly just one file and a folder with images. It uses the [Materialize](https://materializecss.com/) Framework for the GUI and manages to look quite good in my opinion.

![](https://cdn.ant.lgbt/img//antGallery.png)
