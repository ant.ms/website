---
title: " System Monitor API (Python)"
date: 2020-07-28T14:40:00+02:00
draft: false
categories:
  - Sever
tags:
  - API
  - Python
  - Monitor
  - Server
  - Software
icon: https://cdn.ant.lgbt/icons/systemMonitorApi.png
gitlab: https://git.ant.lgbt/ConfusedAnt/system-monitor-api
---

This might not be the coolest ever piece of software, but it certainly can be very useful to have around for when you need it.

It creates a _"REST"_-API, and gives back JSON values that you set before, in order to check on the system remotely (e.g. through a Website/Dashboard). You can see it in action, by going to he [About-Page](/about/), or the [Hosting-Page](/hosting/) on this website.
