---
title: " Intelligo (Ionic)"
date: 2020-08-31T16:20:00+02:00
draft: false
categories:
  - Mobile
tags:
  - HTML
  - CSS
  - JS
  - Typescript
  - Angular
icon: https://cdn.ant.lgbt/icons/Intelligo.png
---

Intelligo is a simple app for learning using index cards. It enables the user to record their own index cards, to learn and to view the associated statistics of the last seven days. Bye Quizlet - Hello Intelligo!

![](https://cdn.ant.lgbt/img//Intelligo.jpg)

You can use to to learn words by a foreign language and enjoy a graphical and picture-based learning experience in the process. I created Intelligo, together with a friend, as an exercise in Ionic.

> Check out on [my Gitlab](https://git.ant.lgbt/experiments/school/m335/intelligo)
