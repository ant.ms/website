---
title: "Polybar Script for checking twitch online status"
date: 2019-05-19T15:07:00+01:00
draft: false
categories:
  - Customisation
tags:
  - bash
  - polybar
  - i3wm
  - Linux
  - script
  - twitch
icon: https://cdn.ant.lgbt/icons/radio.png
gitlab: https://gitlab.com/1Yanik3/polybar-twitch
---

This is a simple bash-script for polybar that checks if a certain twitch-streamer (or multiple) is streaming right now.

<!--more-->

It only works with one active streamer at a time, although if you want you can set the priority (aka: which one of those will be shown) by adding that name to the end of the users-array.

dependencies:

- [twitch-stream-check](https://github.com/joeleisner/twitch-stream-check)

usage/installation:

- download [twitch.sh](https://gitlab.com/1Yanik3/polybar-twitch/blob/master/twitch.sh)
- place the script file in ".config/polybar"
- change the "users"-array to match the usernames that you want to be monitored
- add script to polybar as custom module:

```
[module/twitch]
type = custom/script
exec = /home/host13/.config/polybar/twitch.sh
format-prefix = 
format-prefix-padding = 1
tail = true
```

- add module to bar by putting "twitch" in either modules-left, modules-center or modules-right
- enjoy
