---
title: "Polybar System Monitor Custom Script"
date: 2019-06-24T12:00:00+01:00
draft: false
categories:
  - Customisation
tags:
  - polybar
  - bash
  - Linux
  - System Monitor
  - i3wm
  - script

icon: https://cdn.ant.lgbt/icons/system-monitor.png
---

This is a simple bash-script for polybar that displays system usage of the cpu, ram, swap and gpu(nvidia) with the use of bars.

<!--more-->

dependencies:

- nvidia-smi (only if you want to use the GPU-module

usage/installation:

- download [system-mon.tar.gz](https://cdn.ant.lgbt/files/system-mon.tar.gz)
- place the script files in ".config/polybar"
- add scripts to polybar as custom modules:

```
[module/cpu]
type = custom/script
exec = .config/polybar/cpu.sh
interval = 3

[module/memory]
type = custom/script
exec = .config/polybar/memory.sh
interval = 3

[module/swap]
type = custom/script
exec = .config/polybar/swap.sh
interval = 3

[module/gpu]
type = custom/script
exec = .config/polybar/gpu.sh
interval = 3
```

- add module to bar by putting _cpu_,_memory_,_swap_ and _gpu_ in either "modules-left", "modules-center" or "modules-right"
- add "Unifont" as a font in your polybar configs
  `font-9 = Unifont` (replace 9 with the next unused number)
- enjoy
