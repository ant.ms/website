---
title: "Game of Life"
date: 2021-06-28T10:38:56+02:00
draft: false
categories:
  - Canvas
  - Svelte
  - TypeScript
tags:
  - JavaScript
  - TypeScript
  - Svelte
icon: icons/game-of-life.png
gitlab: https://gitlab.com/AntExperiments/webapps/game-of-life
demo: https://antexperiments.gitlab.io/webapps/game-of-life/
---

The Game of Life is a cellular automaton developed by John Conway in 1970. It is a zero-player game, meaning that after the initial input, the _player_ does not intervene.

<iframe src="https://antexperiments.gitlab.io/webapps/game-of-life/" style="background:#ffffff;border-radius:20px"></iframe>

My main reason for creating this project, is that I was fascinated with it from the moment I saw it.
