---
title: "Space Shooter"
date: 2018-03-01T20:20:56+01:00
categories: [development, game]
tags: [game,scratch,1yanik3]
draft: false
categories:

tags:
- shooter
- 2D
---

Space Shooter is a game I've coded, in two hours, while at Ergon in Switzerland.

<iframe src="https://scratch.mit.edu/projects/186864290/embed" allowtransparency="true" width="720" height="560" frameborder="0" scrolling="no" allowfullscreen></iframe>

Its free to play and doesn't need to be installed, just click this link to open and play this game online or play it in the iframe above (can cause errors though because of your browsers security settings): [scratch.mit.edu](https://scratch.mit.edu/projects/186864290/).
