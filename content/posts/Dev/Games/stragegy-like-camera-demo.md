---
title: "'Stragegy-Like-Camera' (Unity)"
date: 2020-06-15T17:42:12+02:00
draft: false
categories:

tags:
  - Unity
  - Interactive
  - Software
  - Live
icon: https://cdn.ant.lgbt/icons/strategyLikeCameraDemo.png
gitlab: https://gitlab.com/ant.ms/unity-demo-project
demo: https://projects.ant.lgbt/unity-exportCamera/
banner: error
---

This is a demo that I made about camera movement, more specifically, on how to handle strategy-like camera movement in Unity.

<iframe scrolling="no" src="https://projects.ant.lgbt/unity-exportCamera/"></iframe>

As you can see, the demo has some issues and imperfections, it's not meant to be perfect. It serves it's purpose of helping me understand more about Unity.
