---
title: "Text-Based Tic Tac Toe, written in C"
date: 2019-11-27T17:54:12+01:00
draft: false
categories:
tags:
  - C
  - Software
icon: https://cdn.ant.lgbt/icons/TicTacToe.png
gitlab: https://gitlab.com/ant.ms/tictactoe
---

This is a fully functional TicTacToe game that I wrote in C to work in the console, it has no dependencies and is made very simple. Don't expect an amazing gaming experience as this is mainly just a project for myself and not very focused on the user envoriment at all, it's functional but not very UI-focused.
