---
title: "Install a public Key for a SSH connection"
date: 2020-02-17T13:03:42+01:00
draft: post
categories:
tags:
  - Linux
  - SSH
  - server
icon: https://cdn.ant.lgbt/icons/key.png
banner: archived
---

Public Keys are a form of [asymetrical encryption](https://en.wikipedia.org/wiki/Public-key_cryptography) that is basically the industry for non-password-authentification.

<!--more-->

I'm assuming you have already set up a SSH-Server, if you haven't you can take a look [here](https://ant.lgbt/posts/howto/configure-sshd/). Make sure this line is set:

```bash
PubkeyAuthentication yes
```

## Generating a key

Execute `ssh-keygen` and follow the wizard. This will generate a _rsa_-key by default, here is a small comparison if you want to select another one and why you may want so (execute `ssh-keygen -t <nameOfAlgorythm>` in order to select anything else than _RSA_). You want to use the default path in most cases, although if you really want to, you can change it.

| Algorythm&nbsp;&nbsp;&nbsp; | Description                                                                                                                                                                              |
| --------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| rsa                         | A very old alrgorythm, has a keysize between 2048 bits and 4096 bits. It is save to use for now but it is expected to be breached in the near future.                                    |
| dsa                         | An old US based algorythm that uses a keysize of 1024 bits, not recommended for use anymore                                                                                              |
| ecdsa                       | Standardised by the US government, uses keysizes of 256, 384 or 521 bits. It is a lot more efficient thatn rsa but not supported by every device yet (old devices might get some issues) |
| ed25519                     | Very new algorythm introduced by OpenSSH, not suported by everything.                                                                                                                    |

## Copying the public Key to the server

After creating the keypair which will be located under _~/.ssh_ and most likely be called _id_rsa_ for the private key, and _id_rsa.pub_ for the public key. You should pay attention that you're never giving away the private key (_id_rsa_) to anyone, it is used to identify whoever owns it as you and can be very easily used to get access to sensitive information and your server.

There are multiple ways of getting the Public Key to the server. In the end you just have to add a file to the servers _~/.ssh/authorized_keys_ file. That file looks something like this:

```
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDN9xGcvqmkT4gHFBIVHOR7Vjafu0IrRigFEOTyAj....... host13@Workstation
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC6pBKCVNyY3ZLWoQ3rdT7HdMMAlnQ5bAs7AE71PH....... host13@SchoolLaptop
```

Every line represents a seperate clients Public Key, so just copy the contents of your Public Key to your servers _authorized_keys_-file.

Alternatively you can use `ssh-dopy-id -i <pathToYourPublicKey> <serverUsername>@<serverHostname>` to copy it to the server by command line (requires the ability to connect to that server though).
