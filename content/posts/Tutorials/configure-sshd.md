---
title: "Configure and Install an SSH-Server"
date: 2020-02-16T10:51:42+01:00
draft: false
categories:
tags:
  - Linux
  - SSH
  - server
icon: https://cdn.ant.lgbt/icons/network-console.png
banner: archived
---

In my opinion is SSH one of the best pieces of software ever written and probably the most important part of my workflow. Setting up a server isn't hard, but configuring it correctly can be pretty difficult.

<!--more-->

## Installation

First we have to install an SSH server, I'd recommend you install [openssh](https://www.openssh.com/) because it worked really well for me. Just execute the correct install command for your distribution:

Ubuntu/Debian: `sudo apt install openssh-server`<br>
Manjaro/Arch: `sudo pacman -S openssh`

Then verify the service is running: `systemctl status sshd`. The output should look something like this:

```
● ssh.service - OpenBSD Secure Shell server
   Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
   Active: active (running) since Sun 2020-02-16 11:11:06 CET; 1s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
  Process: 21794 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
 Main PID: 21795 (sshd)
    Tasks: 1 (limit: 4915)
   Memory: 1.3M
   CGroup: /system.slice/ssh.service
           └─21795 /usr/sbin/sshd -D

Feb 16 11:11:06 AxxivServer sshd[21795]: Server listening on 0.0.0.0 port 22.
Feb 16 11:11:06 AxxivServer sshd[21795]: Server listening on :: port 22.
```

Make sure that it's running: `Active: active (running)` and that it is set to start when the system starts: `.../lib/systemd/system/ssh.service; enabled...`

That's it, your server is now working. Connect to it from any PC using the _ssh_ command: `ssh [USERNAME]@<IP>`

If you want to access it from the internet, make sure you forward port 22, if you do this, I'd recommend you turn off password-login, at least for external IPs as shown in the configuration section below.

## Useful Configuration

Eventhough the server is now running and working, you can still configure additionial settings which can improve usability and security of your server. The main configuration file for your SSH-Server is _/etc/ssh/sshd_config_, open it with your favourite editor to edit it.

After every change you make, use `sudo sshd -T` to check if the config file is correctly formated and, if no errors where found, `sudo systemctl restart sshd` in order to restart the service.

There are a lot of things you can change, here is a list of useful tweaks:

### Disable password authentification

Password login is the main way hackers can get into your server, they use brute-force and just try every combination possible until they manage to get in. In order to disallow that, you can disable password authentification all together and only use Keys to login to your SSH-Server

> **Make sure that you have a working Pubkey login with your SSH server and at least one client, otherwise you could loose SSH access to your server entirely**

After making sure you have Public Key Authentification enabled and working (you can find out how to do that [here](https://ant.lgbt/posts/howto/public-key-ssh/)) by making sure this line is uncommented or add it manually:

```bash
PubkeyAuthentication yes
```

You can now uncomment/add this line to disable password login via SSH

```bash
PasswordAuthentication no
```

A useful way I recently found in order to get both the benefits of Password Authentication and the security of disabling it, at the same time is to only allow it on the local network and leave it disabled everywhere else (the steps above still need to be completed in order for this to function correctly).

Add this line **at the end of the file** in order to achieve this (change the IP-Range if necesairy, this should work for most home-users though):

```bash
Match Address 192.168.*.*
        PasswordAuthentication yes
```

### Change the Port

This is something I'm not really a fan of doing but a lot of people recommend doing in order to not get hacked as easily. It basically makes it harder for random hackers to figure out that you have a SSH server at all, so they won't just randomly try to brute-force there way into your server.

Uncomment and change this line to whatever port you want:

```bash
Port 22
```
