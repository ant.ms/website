---
title: "Gnomify Firefox"
date: 2020-01-07T17:27:42+01:00
draft: false
categories:
  - Customisation
tags:
  - review
  - customisation
  - Firefox
  - Gnome
  - Linux
  - tweaking
icon: icons/firefox.png
---

Firefox is a great Web-browser, which I've been using for years by now. There's one thing I don't like about it: It's design.

<!--more-->

![Screenshot of my Firefox configuration](https://cdn.ant.lgbt/img//firefox.png)

Don't get me wrong, Firefox doesn't look that bad but I think you can do it better, and because it's Firefox we can change it a lot.

> DISCLAIMER: This only works with Firefox (not Chromium based browsers) and this (exact theme) only works **on Linux** (Tested with Gnome 3.34.2)

There are multiple different themes for Firefox, I tried the [Mojave](https://github.com/vinceliuice/Mojave-gtk-theme/tree/master/src/firefox) and the [Gnome](https://github.com/rafaelmardojai/firefox-gnome-theme) theme for it. They are both kinda similar and both use [userChrome.css](https://www.userchrome.org/) in order to function correctly. In case you don't know what [userChrome.css](https://www.userchrome.org/) is: It's a CSS file (or multiple if you want) that can completely change the way Firefox works and/or renders websites. In this case we only use them to change the look and feel of Firefox but you're free to change and do anything you want.

I won't explain all the steps for installing it here because you can find them on the individual project pages of each theme, but let me tell you, it's so so worth it in my opinion. I wish Chromium based browsers would support this as well tbh.
