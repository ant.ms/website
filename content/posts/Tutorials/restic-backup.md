---
title: "Backup your files with Restic"
date: 2022-03-28T20:00:00+01:00
draft: false
categories:
  - Cheatsheet
tags:
  - Restic
  - Backup
  - Linux
icon: icons/restic.png
---

I've been looking for some good backup solutions for my infrastructure for quite a while now, and I think I finally found one that I'll stick with. If you want to have a closer look at it, here is their website: [restic.net](https://restic.net/).

So why am I using Restic?

1. snapshot based
2. quite easy to use
3. still getting updates (unlike e.g. rsnapshot)
4. quite light-weight, only requiring a single executable
5. compatible with a lot of different storage-backends

I personally am using [Wasabi](https://wasabi.com/) to store my buckets, but

## Preparation

The first step is quite obvious, install restic with your package manager: `sudo apt install restic`.

The next few steps are how I personally like to set up Restic, these can vary based on person though. For my setup, I'll be using S3 storage (just change that to whatever you are using).

Create a file at `/etc/restic_env` with this content:

```bash
export AWS_ACCESS_KEY_ID="<YOUR ACCESS KEY (ID)>"
export AWS_SECRET_ACCESS_KEY="<YOUR SECRET ACCESS KEY>"
export RESTIC_REPOSITORY="s3:<ENDPOINT TO BACKUP TO>/<BUCKET NAME>"
export RESTIC_PASSWORD_FILE=/etc/restic_password
```

Then create another file at `/etc/restic_password`, containing just the password for the bucket (aka: make this a random string but make sure to store it somewhere save). Make sure, that only root can read this file (for security purposes).

## Setup Repository

Run `source /etc/restic_env` (you might have to run `. /etc/restic_env`, as source is not always supported) in order to load the environment variables for restic, and then run `restic init` to initialize the repository. This also has the side-effect, of testing if the variables are set correctly and everything is working.

## Setup Backup Schedule (Crontab)

If you don't know what crontab is, or don't know how to use it, you can find a tutorial from me [here](/posts/cheatsheets/crontab/).

Run `sudo crontab -e`, in order to edit your crontabs. At the bottom of the file, add these lines:

```sh
40 */6 * * * . /etc/restic_env && /snap/bin/restic backup <PATH1> <Path2> <...> && /snap/bin/restic forget --prune -l 8 -d 3 -w 5 -m 4
```

Make sure to change the paths to the actual paths you want to backup (leave out the angle brackets), you can add as many paths as you like. Also be aware, that you can change when this crontabs executes (my example here executes it every 6 hours at 40 minutes past the hour).

You may also change the forget policy (see [restic.readthedocs.io/en/stable/060_forget.html](https://restic.readthedocs.io/en/stable/060_forget.html) for details) in order to change when the snapshots are going to be removed. This is what the letters mean:

| parameter | meaning                           |
| --------- | --------------------------------- |
| -l        | keep the last n snapshots         |
| -H        | keep the last n hourly snapshots  |
| -d        | keep the last n daily snapshots   |
| -w        | keep the last n weekly snapshots  |
| -m        | keep the last n monthly snapshots |
| -y        | keep the last n yearly snapshots  |

## Enable logging

As Crontab doesn't log what is happening by default, I'd suggest you set that up as well (although that is optional).

In order to get that to work, the only thing you need to do is install [postfix](https://www.postfix.org/). You can do this with `sudo apt install postfix
`.

Then, whenever the crontab runs, you'll get the output added to `/var/mail/root` (I'd recommend to use _cat_ or _tail_ to read the file).
