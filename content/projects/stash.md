---
title: "Stash"
date: 2024-02-20T17:30:00+02:00
draft: false
toc: true
summary: A featureful tag based library to organise your media collections
gitlab: https://gitlab.com/ant.ms/stash
icon: icons/stash.png
categories:
  - active
---

Stash is a featureful tag based library to organise your media collections.

## History

A long, long time ago (like a year or two) I used to use [Raindrop](https://raindrop.io/) to manage a lot of media files because it has good tagging functionality, but I ran into limitations, a lack of polish on features I cared about, a 100mb per file limit, and a general slowness of the whole application (to be fair, I don't think it was designed for massive media collections and more for collecting links, but the point still stands). Unfortunately, I couldn't find another alternative that ticked all my boxes.

Of course, I did what any "sensible" person would do and created my own solution, tailored to my personal needs. Little did I know that this "little weekend project" would end up expanding far beyond a minimum viable product.

Please note, that this project is open source, however I do not (yet) recommend you use it. Because part of my initial idea for the project was for it to just be a "weekend project", things are still not implemented in ways that I am fully happy with. Some things are still hardcoded (so not configurable ~~unless you count making a fork as "configuration"~~), and the project is not setup for non-breaking-changes.

<!-- ## UI (and Screenshots!)

TODO -->

## How it works

The application is a [SvelteKit](https://kit.svelte.dev/) project that get's deployed as a Docker Container. I used to use go as the backend, however I decided to stop using that as I have ran into a few issues, and the code became a pain to maintain.

For the Database I use [PostgreSQL](https://www.postgresql.org/) (run as a seperate container), as it has a lot of features and can easily be hosted locally. I access it though [Prisma](https://www.prisma.io/).

## Tag System

The core of Stash is it's tag system. While it is intentionally kept simple, it allows a media object to be assigned to multiple tags. Additionally tags can have more detailed sub-tags. This way you might have a tag called "pets" and then sub-tags like "cat" or "dog". When viewing the tags you can then decide if you want to see both the parent and child tag items, or just the parent tag items.

In addition to a simple label for the tag, they can also have an icon and description assigned to them. In the future I plan to make them even more powerful by allowing child-tags to be non-hierarchical. Meaning you can tag a tag with another tag ~~(try to say that 10 times fast)~~.

### AI Tagging

When you upload a new media file (currently only images but more is planned), Stash will automatically run it against a number of AI-enabled tags. While this might sound complicated but it's actually quite simple.

The tags that the user has already defines can have a description added to them, which is just plain text describing the tag. The worker will then in the background after the import go and send the image to an LLM with a long prompt that includes all tags which have descriptions, along with the description itself. The LLM will then return a list of tags that it thinks are relevant for the image as a structured JSON file.

Using a description system like this has multiple benefits. First it means that not all tags are automatically used by the AI. I've noticed that the models are still quite stupid and are often wrong, so you can tweak the tags that are used by the AI. Second it means you can explain what you mean by that tag to the AI, as the name itself can and will be interpreted in multiple different ways, none of which are what you actually intended.

## Future Plans

I plan on continuing to develop this application, as I am using it myself regularly and find it extremely useful for my purpose. It is not even close to being complete though and has already used a lot of my time though, so don't expect it to be finished anytime soon... or probably ever. Nevertheless here are some of the features that I still plan to add:

- Importer from various web sources<br/>
  <span class="desc">
  Probably using yt-dlp, as well as some auto-import scripts for sites like Patreon or Twitter
  </span>
- Automatic media optimisation<br/>
  <span class="desc">
  A lot of media files aren't well optimised, even without loosing any quality a lot of storage space can be saved
  </span>
- Final goal: Make everything customizable (nothing hardcoded)<br/>
  <span class="desc">
  This would finally allow other people to use the software too, a lot of work is needed though to get the application to this point and at the moment it is unclear for me if I want to putin the effort for (probably) 0 actual users
  </span>

<style>
  .desc {
    display: block;
    font-size: 0.8em;
    line-height: 1.5em;
    margin-bottom: 5px;
  }
</style>
