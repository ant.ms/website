---
title: Projects
titleIcon: puzzle
disableList: true
---

I personally make the destinction between projects, which are found on this page, and articles, which are under the [Blog section](/posts). Articles may also contain some smaller projects, but those found there aren't ones that I plan on maintaining or continuing to develop, as those are usually just experiments or similar things. You can find those under the [Development section of my Blog](/posts/dev/)

Here you will find the larger projects that I actually plan on maintaining. There are not many of them as they are quite time consuming (and - contrary to popular believe - I have other things to do sometimes too), but when I do you can find them here.

## Never-ending projects

{{< float page="/projects/stash" >}}

## Completed™

{{< float page="/projects/auth" >}}

<!-- ## Work in progress

{{< float page="/projects/recipe" >}} -->
