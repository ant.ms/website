---
title: "Auth"
date: 2023-09-29T19:00:00+02:00
draft: false
summary: A self-hosted authentication provider that integrates into Traefik
gitlab: https://gitlab.com/ant.ms/auth
icon: icons/auth.png
categories:
  - completed
---

Auth is a self-hosted authentication provider that integrates into [Traefik](https://traefik.io/). It is designed to be a simple, easy to use, and secure way to secure web applications, that don't have their own authentication system (or I deemed it annoying for them to have their own and wanted to use a common one).

![](/assets/images/posts/auth-sessions-screen.png)

Architecture wise it's built in what for me would be the default by now: It uses [SvelteKit](https://kit.svelte.dev/) and [Prisma](https://www.prisma.io/), and is deployed as a Docker Container, built within [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/).

## How it works

The application acts as a middleware in Traefik, so it can be added to any web application. It itself also has it's own subdomain which it uses to serve the login page and the management interface.

Traefik is configured to check if the request contains a valid session (this is done through an internal request). If the session is not valid (or not present at all), the user will be redirected to the login page, with the origin being remembered as part of the URL.

![](/assets/images/posts/auth-login-page.png)

There the user can either login via a passkey ([webauthn](https://webauthn.io/)) or by scanning the visible QR code with their phone (or another authenticated device). After a successful login, the user is redirected back to the origin and can use the application transparently.
