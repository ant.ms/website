---
title: "Network / Server Hardware"
date: 2022-03-24T20:00:00+01:00
draft: false
banner: false
titleIcon: lan
---

<!-- ![Network Diagram](https://cdn.ant.lgbt/img//Julianik.png) -->

{{< section >}} Hera {{< /section >}}

Hera is my primary server for my personal use. It stores all my files, and quite a few of the services I host for myself. It's being backed up every few hours to [Borgbase](https://www.borgbase.com). It's connected to all my devices through a [Tailscale](https://tailscale.com/) Network, and connected to the internet through a [Cloudflare Tunnel](https://github.com/cloudflare/cloudflared) (so no port forwards are required).

<div class="hardwareTable">
    <div>
        <div>{{< svg "console" >}} OS: Unraid</div>
        <div>{{< svg "cpu" >}} CPU: AMD Ryzen 3700X</div>
        <div>{{< svg "memory" >}} Memory: 64GB DDR4</div>
        <div>{{< svg "hdd" >}} Storage: 16.2TB (M.2/SATA RAID)</div>
    </div>
</div>

It's running [Unraid](https://www.unraid.net/) (used to run [TrueNAS](https://www.truenas.com/)), because it has the ability to extend it's array drive by drive, whenever I need to expand my capacity. Most of the data-access is done through NFS, SMB, and the [Syncthing](https://syncthing.net/) plugin, that has been installed on it.

<!-- {{< section >}} Polus {{< /section >}}

Polus is my primary VPS, which I host at [Contabo](https://contabo.com/en/). It's my main gateway to the internet, and hosts a variety of things using docker containers (managed through Portainer). In addition to that, it also has the job of functioning as a proxy to make some of my internal services (accessible through Zerotier) accessible over the open internet.

<div class="hardwareTable">
    <div>
        <div>{{< svg "console" >}} OS: Ubuntu</div>
        <div>{{< svg "cpu" >}} CPU: 6 Cores</div>
        <div>{{< svg "memory" >}} Memory: 16GB</div>
        <div>{{< svg "hdd" >}} Storage: 400GB SSD</div>
    </div>
</div>

It's quite powerful for what I need it, but I like to have a bit of overkill. Currently the bandwidth is limited to 400 Mbit/s though, which is something I might want to upgrade in the future, because this is essentially the only gateway out of my [Zerotier network](/posts/publish/zerotier/). -->

{{< section >}} My Domains {{< /section >}}

<div style="overflow-x:auto;">

| Domain                                                    | Current use                                                 |
| --------------------------------------------------------- | ----------------------------------------------------------- |
| [ant.lgbt](https://ant.lgbt/) / [ant.ms](https://ant.ms/) | hosts this website                                          |
| [yanik.gay](https://yanik.gay/)                           | idk yet, got it as a gift                                   |
| [verified.gay](https://verified.gay/)                     | currently unused, planning on renting out emails/subdomains |
| [whythisannoys.me](https://whythisannoys.me/)             | in reserve for when I will be very annoyed about something  |
| [any.gay](https://any.gay/)                               | used for personal projects/services                         |

</div>

<!-- {{< section >}} Main Sites I created, hosted and maintain {{< /section >}}

<div style="overflow-x:auto;">

| Site-Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | **URL** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Engine &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |
| ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------- |
| ConfusedAnt                                                                                                      | [ant.lgbt](https://ant.lgbt/) / [ant.ms](https://ant.ms/)                                                                                                                                                                        | Blog                                                        | <i class="fab fa-markdown"></i> Hugo                    |
| ConfusedCloud                                                                                                    | [any.gay](https://ant.gay/)                                                                                                                                                                                                      | Cloud                                                       | <i class="fas fa-cloud"></i> Nextcloud                  |
| Julien Acker                                                                                                     | [julienacker.dev](https://julienacker.dev/)                                                                                                                                                                                      | Cute Page                                                   | <i class="fab fa-html5"></i> HTML                       |
| Alex Heimservice                                                                                                 | [alex-heimservice.ch](http://www.alex-heimservice.ch/)                                                                                                                                                                           | Business                                                    | <i class="fab fa-wordpress-simple"></i> Wordpress       |

</div>

{{< section >}} Websites I created but am no longer working on or maintaining {{< /section >}}

<div style="overflow-x:auto;">

| Site-Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | **URL** &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Type &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | Engine &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; |
| ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------- | ------------------------------------------------------- |
| Visual-Gallery &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                                                                    | [visual-gallery.ch](https://www.visual-gallery.ch/de/)                                                                                                                                                                           | Business                                                    | <i class="fab fa-wordpress-simple"></i> Wordpress       |
| Paradise To Rent                                                                                                 | [paradise-to-rent.com](https://www.paradise-to-rent.com/)                                                                                                                                                                        | Business                                                    | <i class="fab fa-wordpress-simple"></i> Wordpress       |

</div> -->
